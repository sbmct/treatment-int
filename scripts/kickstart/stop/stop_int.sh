#!/bin/bash

d="`dirname $0`"

BIN_PATH=/opt/oblong/sluice-64-2/bin

. "${BIN_PATH}/rubylib.sh"

#kill some daemons
pushd ${BIN_PATH}/int
ruby -I`pwd` -I$(rubylib) trackd.rb stop
ruby -I`pwd` -I$(rubylib) dossierd.rb stop
popd

killall -9 java

killall -9 camera
