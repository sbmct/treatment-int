#!/bin/bash

d="`dirname $0`"

ETC_PATH=/etc/oblong
SHARE_PATH=/usr/share/oblong
BIN_PATH=/opt/oblong/sluice-64-2/bin
GST=/opt/oblong/deps/lib/gstreamer-0.10

t=`date -u +%H`
vids=
for i in {0..23}
do
  h=$((($i + $t) % 24))
  padded_h=`printf "%02d" $h`
  vids="$vids -v ${SHARE_PATH}/vidconf/${padded_h}00.mp4";
done

#run video cam behind sluice and use set of videos starting from the current hour
pushd ${BIN_PATH}/int
export DISPLAY=:0.0

# Wait for X to be available before we try to start camera.
while ! xset q
do
  sleep 2
done

echo "Starting CAMERA"

GST_PLUGIN_PATH=${GST} ./camera $vids ${ETC_PATH}/video/movie-screen.yaml ${ETC_PATH}/video/movie-feld.yaml > /var/log/oblong/sluice/camera.log 2>&1 &
popd

