#!/bin/bash

d="`dirname $0`"

ETC_PATH=/etc/oblong
SHARE_PATH=/usr/share/oblong
BIN_PATH=/opt/oblong/sluice-64-2/bin

. "${BIN_PATH}/rubylib.sh"

#Poke the fluoroscopes
poke edge-to-sluice ${ETC_PATH}/int/inculcators.yaml
#poke edge-to-sluice ${ETC_PATH}/mobile-network.protein
#poke edge-to-sluice ${ETC_PATH}/streetcam-network.protein

#Poke the mobile network
poke topo-to-sluice ${SHARE_PATH}/cell_network/data/topo/cells.yaml

#Poke the prince harry twitter network
poke topo-to-sluice ${SHARE_PATH}/twitter/data/topo/harry_love.yaml

#Poke streetcam network
poke topo-to-sluice ${SHARE_PATH}/streetcam_network/data/topo/cameras.yaml

#Poke navigational change
poke edge-to-sluice ${ETC_PATH}/misc/scale_by_velocity_linear.yaml

# start some daemons
pushd ${BIN_PATH}/int
java -jar twitterd.jar &
ruby -I`pwd` -I$(rubylib) dossierd.rb start
ruby -I`pwd` -I$(rubylib) trackd.rb start
popd
