#!/bin/sh

if [ -d "$HOME/src/sluice/projects/sluice/ruby/rubylib.sh" ] ; then
    . "$HOME/src/sluice/projects/sluice/ruby/rubylib.sh"
else
    . "/opt/mct/sluice/bin/rubylib.sh"
fi
cd `dirname $0`/../../src/ruby/dossier
ruby -I`pwd` -I$(rubylib) dossierd.rb start
trap "ruby -I`pwd` -I$(rubylib) dossierd.rb stop ; exit" 2 3 15
while true ; do
	sleep 60
done

