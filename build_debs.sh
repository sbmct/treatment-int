#!/bin/sh

set -ex

PACKAGE_VERSION="1.20.1"
INT_BRANCH="master"
DEBS_PATH=$(pwd)/debs/sluice-int-network
SHARE_PATH=usr/share/oblong
ETC_PATH=etc/oblong
BIN_PATH=opt/oblong/sluice-64-2/bin
VAR_PATH=var/ob/sluice
WEB_PATH=/opt/oblong/intelligence
VID_PATH=/ob/assets/video/sluice_demos/intelligence
TMP_PATH=tmp

GIT_USER="`id -un`"
BASE_PATH=$(pwd)
echo -n "Enter username for git.mct.io [${GIT_USER}]: "
read guser
if [ "${guser}" != "" ]; then
    GIT_USER=${guser}
fi

#create the control, prerm and postinst files
sed -e "s,@VERSION@,${PACKAGE_VERSION},g" ${DEBS_PATH}/DEBIAN/control.in \
    > ${DEBS_PATH}/DEBIAN/control
sed -e "s,@prefix@,${BIN_PATH},g" ${DEBS_PATH}/DEBIAN/postinst.in \
    > ${DEBS_PATH}/DEBIAN/postinst
chmod +x ${DEBS_PATH}/DEBIAN/postinst
sed -e "s,@prefix@,${BIN_PATH},g" ${DEBS_PATH}/DEBIAN/prerm.in \
    > ${DEBS_PATH}/DEBIAN/prerm
chmod +x ${DEBS_PATH}/DEBIAN/prerm

# Remove the old files

cond_remove() {
  test -d "$1" && rm -fr "$1"
}

cond_remove ${DEBS_PATH}/${SHARE_PATH}
cond_remove ${DEBS_PATH}/${ETC_PATH}
cond_remove ${DEBS_PATH}/${BIN_PATH}
cond_remove ${DEBS_PATH}/${VAR_PATH}
cond_remove ${DEBS_PATH}/${WEB_PATH}

#make the etc and bin directories (share gets made with the git checkout)
mkdir -p ${DEBS_PATH}/${ETC_PATH}
mkdir -p ${DEBS_PATH}/${BIN_PATH}
mkdir -p ${DEBS_PATH}/${BIN_PATH}/int
mkdir -p ${DEBS_PATH}/${VAR_PATH}
mkdir -p ${DEBS_PATH}/${WEB_PATH}
mkdir -p ${DEBS_PATH}/${TMP_PATH}

echo "checkout the latest intelligence demo"
# checkout the latest from the defined branch
git archive --format=tar --prefix=${SHARE_PATH}/ \
    ${INT_BRANCH} | tar -C ${DEBS_PATH} -x

#build the camera source
(cd "${DEBS_PATH}/${SHARE_PATH}/src/c++/libEZ" && make)
(cd "${DEBS_PATH}/${SHARE_PATH}/src/c++/camera" && make)

# move files into their final destinations and remove unnecessary files
mv ${DEBS_PATH}/${SHARE_PATH}/etc/* ${DEBS_PATH}/${ETC_PATH}/.
mv ${DEBS_PATH}/${SHARE_PATH}/scripts/* ${DEBS_PATH}/${BIN_PATH}/.
mv ${DEBS_PATH}/${SHARE_PATH}/share/* ${DEBS_PATH}/${SHARE_PATH}/.
mv ${DEBS_PATH}/${SHARE_PATH}/mummies ${DEBS_PATH}/${VAR_PATH}/.
mv ${DEBS_PATH}/${SHARE_PATH}/web/* ${DEBS_PATH}/${WEB_PATH}/.

#move executables to the final bin location
mv ${DEBS_PATH}/${SHARE_PATH}/src/c++/camera/camera ${DEBS_PATH}/${BIN_PATH}/int/.
mv ${DEBS_PATH}/${SHARE_PATH}/src/ruby/dossier/* ${DEBS_PATH}/${BIN_PATH}/int/.

#move the offline gems into place
mv ${DEBS_PATH}/${SHARE_PATH}/install/daemons-1.1.9.gem ${DEBS_PATH}/${TMP_PATH}/.

#remove no longer necessary paths
rm -rf ${DEBS_PATH}/${SHARE_PATH}/etc
rm -rf ${DEBS_PATH}/${SHARE_PATH}/scripts
rm -rf ${DEBS_PATH}/${SHARE_PATH}/debs
rm -rf ${DEBS_PATH}/${SHARE_PATH}/share
rm -rf ${DEBS_PATH}/${SHARE_PATH}/web
rm -rf ${DEBS_PATH}/${SHARE_PATH}/src
rm -rf ${DEBS_PATH}/${SHARE_PATH}/docs
rm -rf ${DEBS_PATH}/${SHARE_PATH}/install
rm ${DEBS_PATH}/${SHARE_PATH}/build_debs.sh

#scp the video resources from git
echo "pulling in video resources"
rsync -avessh ${GIT_USER}@git.oblong.com:${VID_PATH}/* ${DEBS_PATH}/${SHARE_PATH}/int/videos/.

# create the deb
dpkg-deb -b debs/sluice-int-network "$@"
