#!/bin/sh

root=$(cd `dirname $0` && echo $PWD)

p-create twitter
p-create track
p-create movie

set -e

make -C "${root}/src/c++"

