VENVDO=$(PWD)/scripts/venvdo
FLUORO_DIR=$(PWD)/share/
MEDIA_FILES=$(FLUORO_DIR)/static_images $(FLUORO_DIR)/videos

all: deps $(MEDIA_FILES)

deps: $(PWD)/venv/bin/activate

$(PWD)/venv/bin/activate: requirements.txt
	test -d $(PWD)/venv/bin || virtualenv $(PWD)/venv
	$(VENVDO) pip install numpy
	$(VENVDO) pip install -r $^
	echo "/usr/local/Cellar/py2cairo/1.10.0/lib/python2.7/site-packages/" > venv/lib/python2.7/site-packages/cairo.pth

initialize: deps
	#TODO: After SLSC-110 is merged, update this to a single folder
	cp mummies/*.protein ~/.sluice/var/sluice/mummies/
	cp mummies/*.protein ~/.sluice/var/int/

$(FLUORO_DIR)/static_images:
	mkdir -p $(FLUORO_DIR)/static_images

$(FLUORO_DIR)/videos:
	mkdir -p $(FLUORO_DIR)/videos

clean:
	rm -fr $(PWD)/venv

