require 'rubygems'
require 'metabolizer'
require 'optparse'

outpool = 'tcp://localhost/edge-to-sluice'
inpool = 'tcp://localhost/sluice-to-edge'

# Parse options using OptionParser
optparse = OptionParser.new do |opt|
  opt.on('--outpool=OPTIONAL', '-o', String, "Edge-To-Sluice Pool (#{outpool})") { |x| outpool = x }
  opt.on('--inpool=OPTIONAL', '-i', String, "Sluice-To-Edge Pool (#{inpool})") { |x| inpool = x }
end
optparse.parse!

class DossierDaemon < Metabolizer
  def initialize (inpool, outpool)
    super inpool
    @inpool = inpool
    @outpool = outpool
    appendMetabolizer(['sluice', 'prot-spec v1.0', 'psa', 'poked-it' ]) { |p| processPoke (p) }
  end

  def processPoke (protein)
    begin
      ing = protein.ingests
      if ing['kind'] == 'Tweet'
        username = ing['attrs']['username']

        out_ing = { :windshield => [ { :name => 'Dossier',
                                       :url => 'http://localhost:7787/sluice/profiler',
                                       :feld => 'right',
                                       :loc => [0.0, 0.3],
                                       :size => [0.4, 1.0]
                                     } ] }
         p = Protein.new(['sluice', 'prot-spec v1.0', 'request', 'web'], out_ing)
         h = Pool.participate @outpool
         h.deposit p
      end
    rescue
      puts 'Could not process protein'
    end
  end

end

dossier_d = DossierDaemon.new(inpool, outpool)

# Trap the System Exit signal
trap_sigint(dossier_d) do
  puts 'Ending DossierDaemon'
end

dossier_d.metabolize 0.1
