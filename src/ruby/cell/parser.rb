require 'csv'
require 'optparse'
require 'Pool'
include Plasma

# set default values
inputfile = 'cells.txt'
outpool = 'topo-to-sluice'
kind = 'Cell'
minlon = -180.0
minlat = -90.0
maxlon = 180.0
maxlat = 90.0

# Parse options using OptionParser
optparse = OptionParser.new do |opt|
  opt.on('--inputfile=MANDATORY', '-i', String, "Input File (#{inputfile})") { |x| inputfile = x }
  opt.on('--outpool=OPTIONAL', '-o', String, "Output Pool (#{outpool})") { |x| outpool = x }
  opt.on('--kind=OPTIONAL', '-k', String, "What to call the kind (#{kind})") { |x| kind = x }
  opt.on('--minlat=OPTIONAL', Float, "minlat (#{minlat})") { |x| minlat = x }
  opt.on('--minlon=OPTIONAL', Float, "minlat (#{minlon})") { |x| minlon = x }
  opt.on('--maxlat=OPTIONAL', Float, "minlat (#{maxlat})") { |x| maxlat = x }
  opt.on('--maxlon=OPTIONAL', Float, "minlat (#{maxlon})") { |x| maxlon = x }
end
optparse.parse!

hose = Pool.participate outpool

#Format of cells:
#id,lat,lon,mcc,mnc,lac,cellid,range,nbSamples,created_at,updated_at
des = [ :sluice, "prot-spec v1.0", :topology, :add, "1/1"]
topology = []
count = 0

def deposit hose, des, ing
  p = Protein.new(des, ing)
  hose.deposit p
  puts p
end

CSV.open(inputfile,'r') do |row|
  if(row[1].to_f > minlat and row[1].to_f < maxlat and
     row[2].to_f > minlon and row[2].to_f < maxlon)
    ing = { :id => row[6],
            :loc => [row[1].to_f, row[2].to_f , 0.0],
            :kind => kind,
            :timestamp => row[10],
            :attrs => {
            :mcc => row[3],
            :mnc => row[4],
            :lac => row[5],
            :range => row[7],
            :samples => row[8],
            :created_at => row[9],
            :updated_at => row[10] } }
    topology << ing
    count = count + 1
    if count % 10000 == 0
      deposit(hose, des, {:topology => topology})
      count = 0
      topology = []
      # break #if you want only one time
    end
  end
end
