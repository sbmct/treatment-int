#ifndef _NAMED_GRIPES_H_
#define _NAMED_GRIPES_H_

#include <libLoam/c++/Str.h>

using namespace oblong::loam;

// These gripes are recognized by the gloveless pipeline
extern const Str GripeNull                       = "_____";
extern const Str GripePushback                   = "||||-";
extern const Str GripeOpenHand                   = "\\/\\/-";
extern const Str GripeOneFingerPointOpen         = "^^^|-";
extern const Str GripeOneFingerPointClosedCurled = "^^^|>";
extern const Str GripeFist                       = "^^^^>";
extern const Str GripeVictory                    = "^^\\/>";

// This one is not
extern const Str GripeOneFingerPointClosed = "^^^||";

// Orientation in a gloveless context is always **
extern const Str GripeOrientationAny       = ":**";

// TODO rest of orientations (palm and fingers)

#endif
