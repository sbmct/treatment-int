
/* (c)  Oblong Industries */


#ifndef ROUNDED_VIDQUAD
#define ROUNDED_VIDQUAD

#include <libNoodoo/VidQuad.h>
#include <libNoodoo/GLVideoFrame.h>

using namespace oblong::noodoo;


class RoundedVidQuad  :  public VidQuad
{ PATELLA_SUBCLASS (RoundedVidQuad, VidQuad);

 private:
  Vect north, east;
  float32 texL;
  float32 texR;
  float32 texT;
  float32 texB;
  Vect v;
  float radius;
  void __inline__ SetEasyVertex (float64 over, float64 up);

 public:
  RoundedVidQuad ();
  void SetRadius (float64 r);
  void DrawSelf (VisiFeld *vf, Atmosphere *atm);
  double Radius ();
};

#endif  // ROUNDED_VIDQUAD