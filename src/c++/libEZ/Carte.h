
/* (c)  Oblong Industries */


#ifndef CARTE_AND_RELATED_STUFF
#define CARTE_AND_RELATED_STUFF

#include <libLoam/c++/ObMap.h>
#include <libBasement/ob-logging-macros.h>
#include <libBasement/KneeObject.h>
#include <libBasement/AsympFloat.h>
#include <libBasement/AsympVect.h>
#include <libBasement/QuadraticVect.h>
#include <libBasement/LinearColor.h>
#include <libBasement/AsympColor.h>
#include <libBasement/SineFloat.h>
#include <libBasement/TimedTrigger.h>
#include <libNoodoo/GLMiscellany.h>
#include <libNoodoo/ObStyle.h>
#include <libNoodoo/TexQuad.h>
#include <libNoodoo/GlyphoString.h>
#include <libImpetus/OEComprehensive.h>
#include <libImpetus/OEPointing.h>
#include <libAfferent/RoddedGlimpser.h>
#include <libAfferent/Gestator.h>
#include <float.h>

using namespace oblong::noodoo;

#define LEFT    (Vect ( 1,  0, 0))
#define RIGHT   (Vect (-1,  0, 0))
#define TOP     (Vect ( 0, -1, 0))
#define BOTTOM  (Vect ( 0,  1, 0))
#define ZERO    (Vect ( 0,  0, 0))

class CarteItem  :  public TexQuad
{ PATELLA_SUBCLASS (CarteItem, TexQuad);

 private:

  TexQuad *itemBacking;
  double animationPeriod;
  AsympColor *color_soft;
  QuadraticVect *loc_soft;
  double scalingFactor;

 public:

  TexQuad *Backing ();
  AsympColor *ColorSoft ();
  QuadraticVect *LocSoft ();
  static void Init (TexQuad *t, FlatThing *parent, Str filename, Str name, float scale);
  CarteItem (int ind, Str filename, Str name, double size,
        Str backingFilename, FlatThing *parent, double sFactor);

};


class BackedGlyph  :  public GlyphoString
{ PATELLA_SUBCLASS (BackedGlyph, GlyphoString);

 private:

  unsigned char *texture;
  int texHeight, texWidth;
  bool texture_created;
  GLuint textureN;
  GlyphoString *shadow;
  bool updateString;
  double shadowOffset;
  ObColor glyphAdjColor;
  ObColor glyphShadowAdjColor;
  ObColor vanishAdjColor;
  ObColor glyphBackingAdjColor;
  Str newStr;
  double scalingFactor;
  bool visible;
  FlatThing *reference;

 public:

  void SetBaseColor (ObColor c);
  void SetShadowColor (ObColor c);
  void SetBackingColor (ObColor c);
  void SetVanishColor (ObColor c);
  static void InitGlyphoString (GlyphoString *g, FlatThing *ft, Str text, double size,
        Vect where, ObColor color = ObStyle::TextColor ());
  ObRetort Travail (Atmosphere *atm);
  BackedGlyph (double relativeFontSize, FlatThing *parent, double sFactor);
  void Appear (Vect where, Str str, FlatThing::H::Align ha = FlatThing::H::Center);
  void Vanish ();
  virtual void DrawSelf (VisiFeld *v, Atmosphere *a);

};


class Chain  :  public ShowyThing
{ PATELLA_SUBCLASS (Chain, ShowyThing);

 protected:

  int visibility;
  unsigned char *texture;
  int texHeight, texWidth;
  bool texture_created;
  GLuint texturen;
  double linkSize;
  SineFloat *sif;  //  respiration soft
  ObColor adjColor;
  double linkSpacing;
  FlatThing *reference;
  double scalingFactor;

 public:

  Chain (Str tFilename, double ls, FlatThing *ref, double sFactor);
  void SetLinkSpacing (double ls);
  void SetBaseColor (ObColor c);
  SineFloat *Sine ();
  void SetVisibility (int v);
  void DecreaseVisibility ();
  int Visibility ();
  void ChangeColor (ObColor color);
  void BeginDrawLinks (ObColor color);
  virtual void DrawSelf (VisiFeld *v, Atmosphere *a);
  void FinishDrawLinks ();
  void DrawLink (Vect center);

};


class CircularChain  :  public Chain
{ PATELLA_SUBCLASS (CircularChain, Chain);

 private:

  double angle1, angle2;
  double radius;
  Vect center;
  ObColor secondColor;
  double fraction;

 public:

  CircularChain (Str tFilename, double ls, FlatThing *parent, double sFactor);
  void SetSecondaryColor (ObColor c);
  double Fraction ();
  void SetFraction (double f);
  void SetCenter (Vect c);
  void SetRadius (double r);
  double StartAngle ();
  double EndAngle ();
  void SetStartAngle (double a);
  void SetEndAngle (double a);
  void DrawCircularSector (double from_angle, double to_angle);
  void DrawSelf (VisiFeld *v, Atmosphere *a);

};


class ParabolaChain  :  public Chain
{ PATELLA_SUBCLASS (ParabolaChain, Chain);

 private:

  Vect ext1, ext2;

 public:

  ParabolaChain (Str tFilename, double ls, FlatThing *parent, double sFactor);
  Vect StartPoint ();
  Vect EndPoint ();
  void SetStartPoint (Vect p);
  void SetEndPoint (Vect p);
  void DrawParabolaLinks (Vect vfrom, Vect vto);
  void DrawSelf (VisiFeld *v, Atmosphere *a);

};


class SinuousChain  :  public Chain
{ PATELLA_SUBCLASS (SinuousChain, Chain);

 private:

  Vect ext1, ext2;

 public:

  SinuousChain (Str tFilename, double ls, FlatThing *parent, double sFactor);
  Vect StartPoint ();
  Vect EndPoint ();
  void SetStartPoint (Vect p);
  void SetEndPoint (Vect p);
  void DrawSinuousLinks (Vect a, Vect b);
  void DrawSelf (VisiFeld *v, Atmosphere *a);

};


class SelectedItemTimedTrigger  :  public TimedTrigger
{ PATELLA_SUBCLASS (SelectedItemTimedTrigger, TimedTrigger);

 private:

  KneeObject *selected_item;
  KneeObject *carte;

 public:

  SelectedItemTimedTrigger (KneeObject *mi, KneeObject *ca);
  ObRetort Fire (KneeObject *self, Atmosphere *atm);

};


class Carte  :  public FlatThing,
                public OEComprehensive::Evts
{ PATELLA_SUBCLASS (Carte, FlatThing);

 protected:

  double itemsSize, selectedItemSize;
  Str itemBackingFilename;
  FatherTime *htime;
  double COUNT;
  double margin;
  ObColor chainAdjColor;
  ObColor hoverAdjColor;
  ObColor selectedAdjColor;
  ObColor itemAdjColor;
  ObColor itemBackingAdjColor;
  ObColor vanishAdjColor;
  bool valid;
  ObTrove <Str> labels;
  ObTrove <Str> names;
  Vect anchor;  //  the harden hit point, or, anyway, the origin of the menu
  bool visible;
  int menu_item;  //  selected menu item
  Vect direction;
  Str chainLinkFilename;
  double chainLinkSize;
  CarteItem *selected;  //  selected item
  BackedGlyph *glyph;  //  the label, one per menu
  ParabolaChain *parabolaChain;
  static ObMap <Str, Carte *> use_map;
  Vect last_ve;
  double scalingFactor;

 public:

  double ScalingFactor ();
  double ItemsSize ();
  static FlatThing *FeldSet (VisiFeld *lvf, VisiFeld *mvf, VisiFeld *rvf);
  void SetMargin (double m);
  Vect Intersection (Vect origin, Vect through);
  Vect IntersectionOfObjectWithGivenFeldPlane (Vect o, SpaceFeld *vf);
  ObRetort DisownProvenance ();
  ObRetort AdoptProvenance (const Str &prv);
  Carte *FindByProvenance (const Str &prov);
  Carte (FlatThing *ref, double iSize,
        Str iBackingFilename, double sFactor);
  virtual void MakeGlyphAppearByItem (BackedGlyph *bl, CarteItem *mi, Vect here, Vect dir);
  virtual Vect ComputeItemLoc (Vect center, int i);
  virtual void MakeItemAppear (CarteItem *mi, Vect there);
  virtual void MakeItemVanish (CarteItem *mi);
  virtual bool UpdateDirectionAndValidity (Vect hit);
  virtual void WhenPointingInsideFelds ();
  virtual bool MenuShouldAppear (Vect hit);
  virtual void UpdateHoverCounter (Vect ve);
  virtual bool VerifyIfCancel (Vect hit);
  virtual bool SpecialSoftenCancel ();
  virtual void HasAppeared (Atmosphere *atm);
  virtual void SelectionChangedToItem (Atmosphere *atm);
  virtual void SelectionChangedToCancel (Atmosphere *atm);
  virtual void HasCanceled (Atmosphere *atm);
  virtual void HasTriggeredItem (Atmosphere *atm);
  void Appear ();
  void Vanish ();
  void AppendItem (Str label, Str name, Str itemFilename);
  bool Summon (Str provenance, Vect origin, Vect through, Atmosphere *atm);
  ObRetort OEPointingHarden (OEPointingHardenEvent *e, Atmosphere *atm);
  ObRetort OEPointingVanish (OEPointingVanishEvent *e, Atmosphere *atm);
  ObRetort Travail (Atmosphere *atm);
  double OverLeft ();
  double OverRight ();
  double UpTop ();
  double UpBottom ();
  virtual int FindSelectedItem (Vect origin, Vect through);
  bool Update (Str provenance, Vect origin, Vect through, Atmosphere *atm);
  ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm);
  bool Dismiss (Str provenance, Atmosphere *atm);
  ObRetort OEPointingSoften (OEPointingSoftenEvent *e, Atmosphere *atm);
};


class DockCarte  :  public Carte
{ PATELLA_SUBCLASS (DockCarte, Carte);

 private:

  double spacing;
  bool wasCancel;
  static SinuousChain *hoverChain;

 public:

  DockCarte (FlatThing *ref, double iSize,
        Str iBackingFilename, double sFactor);
  void SetDirection (Vect dir);
  bool SpecialSoftenCancel ();
  void WhenPointingInsideFelds ();
  bool AlreadyVisible ();
  void UpdateHoverCounter (Vect ve);
  bool MenuShouldAppear (Vect hit);
  bool VerifyIfCancel (Vect hit);
  void MakeItemAppear (CarteItem *mi, Vect there);
  Vect ComputeItemLoc (Vect center, int i);
  void MakeItemVanish (CarteItem *mi);
  void MakeGlyphAppearByItem (BackedGlyph *bl, CarteItem *mi, Vect here, Vect dir);
  bool UpdateDirectionAndValidity (Vect hit);

};


#define VERTICAL    1
#define HORIZONTAL  2

class FloatingCarte  :  public Carte
{ PATELLA_SUBCLASS (FloatingCarte, Carte);

 private:

  double spacing;
  double offset;
  SinuousChain *cancelChain;
  int orientation;

 public:

  FloatingCarte (FlatThing *ref, double iSize,
        Str iBackingFilename, double sFactor);
  void SetOrientation (int o);
  bool MenuShouldAppear (Vect hit);
  bool VerifyIfCancel (Vect hit);
  void MakeItemAppear (CarteItem *mi, Vect there);
  Vect ComputeItemLoc (Vect center, int i);
  void MakeItemVanish (CarteItem *mi);
  void MakeGlyphAppearByItem (BackedGlyph *bl, CarteItem *mi, Vect here, Vect dir);
  bool UpdateDirectionAndValidity (Vect hit);

};

class RadialCarte  :  public Carte
{ PATELLA_SUBCLASS (RadialCarte, Carte);

 private:

  Vect targetLoc;
  CircularChain *cancelChain;
  CircularChain *hoverChain;

 public:

  RadialCarte (FlatThing *ref, double iSize,
        Str iBackingFilename, double sFactor);
  void SetCenterAndDirection (Vect c, bool cancel_on_left = true);
  bool AlreadyVisible ();
  CircularChain *AlreadyHovered ();
  bool MenuShouldAppear (Vect hit);
  void UpdateHoverCounter (Vect ve);
  bool VerifyIfCancel (Vect hit);
  void MakeItemAppear (CarteItem *mi, Vect there);
  Vect ComputeItemLoc (Vect center, int i);
  void MakeItemVanish (CarteItem *mi);
  void MakeGlyphAppearByItem (BackedGlyph *bl, CarteItem *mi, Vect here, Vect dir);
  bool UpdateDirectionAndValidity (Vect hit);

};

#endif  // CARTE_AND_RELATED_STUFF
