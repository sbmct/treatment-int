
/* (c)  Oblong Industries */

#ifndef REMOTE_UTILS_FOR_SELFISH_PLASMA
#define REMOTE_UTILS_FOR_SELFISH_PLASMA

#include <iostream>
#include "libPlasma/c++/Plasma.h"
#include <libLoam/c++/Str.h>
#include <libLoam/c/ob-rand.h>

using namespace oblong::loam;
using namespace oblong::plasma;

Hose *hose_remote;
Hose *hose_remote_data;

/** This enum defines the three possible pointing states.  */
typedef enum
  { NoTouch,     /** The user is not touching the display.  */
    Touch,       /** The user is touching the display.  */
    TapAndTouch  /** The user is hardening.  */
  } TouchType;

/** This enum defines the nature of the received pointing updates.
 *  Simply it is kRelative for iOS devices, and kAbsolute in js.  */
typedef enum
  { Relative,    /** Pointing updates are relative (iOS, Android).  */
    Absolute     /** Pointing updates are absolute (js, wiimotes).  */
  } ModeType;

#define NO_SWIPE_UTTERANCE      "no swipe"
#define SWIPE_UP_UTTERANCE      "swipe up"
#define SWIPE_RIGHT_UTTERANCE   "swipe right"
#define SWIPE_DOWN_UTTERANCE    "swipe down"
#define SWIPE_LEFT_UTTERANCE    "swipe left"

/** Utterances for the discrete swipe blurts. We might want to change the strings.  */
static Str SwipeUtterances[5] =
  { Str (NO_SWIPE_UTTERANCE),      /** This should never be used.  */
    Str (SWIPE_UP_UTTERANCE),      /** Discrete swipe up.  */
    Str (SWIPE_RIGHT_UTTERANCE),   /** Discrete swipe right.  */
    Str (SWIPE_DOWN_UTTERANCE),    /** Discrete swipe down.  */
    Str (SWIPE_LEFT_UTTERANCE)     /** Discrete swipe left.  */
  };

/**  Version string for the proteins format.  */
#define REMOTE_PROTEINS_FORMAT_VERSION "version.2.0"
/**  Name of the pool used to exchange messages with the mobile device or web interface.  */
#define REMOTE_POOL_NAME    "remote"
//  pool used to receive uploaded images and other input from the mobile device
#define REMOTE_DATA_POOL_NAME   "remote-data"
/**  Time before a pointing tracker entity dies completely.  */

Protein GetNextProteinFromRemotePool ()
{ return hose_remote -> Next (Hose::WAIT); }

Protein GetNextProteinFromRemoteDataPool ()
{ return hose_remote_data -> Next (Hose::WAIT); }
      
void SetupPools (const Str &hostname_with_pools = "")
{ printf ("********************************************************\n");
  printf ("*                                                      *\n");
  printf ("*  It is mandatory to run                              *\n");
  printf ("*    pool-server-zeroconf-adapter with argument:       *\n");
  printf ("*        -t remote               (on Linux)            *\n");
  printf ("*    or                                                *\n");
  printf ("*        -t _remote               (on Mac OS X)        *\n");
  printf ("*  (otherwise the service will not be found)           *\n");
  printf ("*                                                      *\n");
  printf ("********************************************************\n");
  
  Str full_remote_pool_name;
  Str full_remote_data_pool_name;

  if (hostname_with_pools == "")
    { full_remote_pool_name = REMOTE_POOL_NAME;
      full_remote_data_pool_name = REMOTE_DATA_POOL_NAME;
    }
  else
    { full_remote_pool_name = "tcp://" + hostname_with_pools + "/" + REMOTE_POOL_NAME; 
      full_remote_data_pool_name = "tcp://" + hostname_with_pools + "/" + REMOTE_DATA_POOL_NAME;
    }
  printf ("Using remote pool: %s\n", full_remote_pool_name.u8);
  printf ("Using remote data pool: %s\n", full_remote_data_pool_name.u8);

  ObRetort ret;
  //  create or (if already existing) just participate in the remote pool
  hose_remote = Pool::Participate (full_remote_pool_name, Pool::MMAP_SMALL, &ret);
  if (ret . IsError ())
    OB_FATAL_ERROR ("Couldn't create or participate in remote pool because '%s'\n",
                    ret . Description () . utf8 ());

  //  create or (if already existing) just participate in the remote-data pool
  hose_remote_data = Pool::Participate (full_remote_data_pool_name, Pool::MMAP_MEDIUM, &ret);
  if (ret . IsError ())
    OB_FATAL_ERROR ("Couldn't create or participate in remote-data pool because '%s'\n",
                    ret . Description () . utf8 ());

}

bool ProteinMatches (const Protein &prt, const Str &des)
{ Str first_descrip;
  if (prt . Descrips () . Nth (0) . Into (first_descrip))
    if (first_descrip == des)
      return true;
  return false;
}

void MetabolizePointing (const Protein &prt)
{ if (ProteinMatches (prt,"remote-pointing"))
  { Slaw ing = prt . Ingests ();
    Str prov;
    ing . MapFind ("provenance") . Into (prov);
    int number_of_touches;
    ing . MapFind ("number-of-touches") .
          Into (number_of_touches);
    int touch;
    ing . MapFind ("touch") . Into (touch);
    int ratchet_state;
    ing . MapFind ("ratchet_state") . Into (ratchet_state);
    Vect vect;
    ing . MapFind ("vect") . Into (vect);
    int mode = 0;
    ing . MapFind ("mode") . Into (mode);
    if (mode == Absolute)
      { // ...
      }
    else  // relative
      { // ...
      }
    double swipe_x = .0;
    double swipe_y = .0;
    ing . MapFind ("swipe_x") . Into (swipe_x);
    ing . MapFind ("swipe_y") . Into (swipe_y);
    int swipe_direction = 0;
    ing . MapFind ("swipe_direction") . Into (swipe_direction);
    double angle_deg;
    ing . MapFind ("angle") . Into (angle_deg);
  }
}

void MetabolizeHello (const Protein &prt)
{ if (ProteinMatches (prt, "remote-hello"))
  { Slaw ing = prt . Ingests ();
    Str provenance;
    Str version;
    Str unique_identifier;
    Str interaction_mode;
    Str name;
    Str model;
    Str os;
    ing . MapFind ("provenance") . Into (provenance);
    ing . MapFind ("version") . Into (version);
    ing . MapFind ("model") . Into (model);
    ing . MapFind ("unique-identifier") . Into (unique_identifier);
    ing . MapFind ("name") . Into (name);
    ing . MapFind ("interaction-mode") . Into (interaction_mode);
    ing . MapFind ("os") . Into (os);
    // ...
  }
}

void MetabolizeResponse (const Protein &prt)
{ if (ProteinMatches (prt, "remote-response"))
  { Str provenance;
    Str version;
    Str response;
    Str conversation_id;
    prt . Ingests () . MapFind ("provenance") . Into (provenance);
    prt . Ingests () . MapFind ("version") . Into (version);
    prt . Ingests () . MapFind ("response") . Into (response);
    prt . Ingests () . MapFind ("conversation-id") . Into (conversation_id);
    printf ("MetabolizeResponse\n");
    printf ("%s\n", provenance.u8);
    printf ("%s\n", version.u8);
    printf ("%s\n", response.u8);
    // ...
  }
}

void MetabolizeImageUpload (const Protein &prt)
{ if (ProteinMatches (prt, "remote-image-upload"))
  { Slaw ing = prt . Ingests ();
    int width;
    int height;
    int orientation;
    Str provenance;
    Str image_format;
    Str conversation_id;
    ing . MapFind ("conversation-id") . Into (conversation_id);
    ing . MapFind ("image-format") . Into (image_format);
    ing . MapFind ("provenance") . Into (provenance);
    ing . MapFind ("image-width") . Into (width);
    ing . MapFind ("image-height") . Into (height);
    ing . MapFind ("image-orientation") . Into (orientation);
    printf ("MetabolizeImageUpload");
    printf ("%s\n", image_format.u8);
    printf ("%s\n", provenance.u8);
    printf ("image upload %d %d %d\n", width, height, orientation);
  }
}

void MetabolizeImageUploadAnnouncement (const Protein &prt)
{ if (ProteinMatches (prt, "remote-image-upload-announcement"))
  { Slaw ing = prt . Ingests ();
    int width;
    int height;
    int orientation;
    Str provenance;
    Str image_format;
    Str conversation_id;
    ing . MapFind ("conversation-id") . Into (conversation_id);
    ing . MapFind ("image-format") . Into (image_format);
    ing . MapFind ("provenance") . Into (provenance);
    ing . MapFind ("image-width") . Into (width);
    ing . MapFind ("image-height") . Into (height);
    ing . MapFind ("image-orientation") . Into (orientation);
    printf ("MetabolizeImageUploadAnnouncement\n");
    printf ("%s\n", image_format.u8);
    printf ("image upload announcement (provenance: %s) %d %d %d\n",
          provenance . utf8 (), width, height, orientation);
  }
}

void MetabolizeTextfieldEdit (const Protein &prt)
{ if (ProteinMatches (prt, "remote-textfield-edit"))
  { Slaw ing = prt . Ingests ();
    Str provenance;
    Str text;
    Str conversation_id;
    ing . MapFind ("conversation-id") . Into (conversation_id);
    ing . MapFind ("text") . Into (text);
    ing . MapFind ("provenance") . Into (provenance);
    printf ("MetabolizeTextfieldEdit: ");
    printf ("%s\n", text.u8);
    printf ("%s\n", provenance.u8);
  }
}

void MetabolizeTextfield (const Protein &prt)
{ if (ProteinMatches (prt, "remote-textfield"))
  { Slaw ing = prt . Ingests ();
    Str provenance;
    Str text;
    Str conversation_id;
    ing . MapFind ("conversation-id") . Into (conversation_id);
    ing . MapFind ("text") . Into (text);
    ing . MapFind ("provenance") . Into (provenance);
    printf ("MetabolizeTextfield: ");
    printf ("%s\n", text.u8);
    printf ("%s\n", provenance.u8);
  }
}

Str ExtractRecipientFromProtein (const Protein &p)
{ Str recipient;
  p . Ingests () . Find ("provenance") . Into (recipient);
  printf ("extracted recipient = %s\n", recipient.u8);
  return recipient;
}

Str ExtractVersionFromProtein (const Protein &p)
{ Str version;
  p . Descrips () . Nth (1) . Into (version);
  version . Replace (0, 8, "");
  return version;
}

Str RecipientDescrip (const Str &recipient)
{ return "recipient:" + recipient; }

char *CreateGUID ()
{ char *b = new char[32 + 4 + 1];
  int32 r1 = ob_rand_int32 (INT32_MIN, INT32_MAX);
  int32 r2 = ob_rand_int32 (0, INT16_MAX);
  int32 r3 = ob_rand_int32 (0, INT16_MAX);
  int32 r4 = ob_rand_int32 (0, INT16_MAX);
  int32 r5 = ob_rand_int32 (0, INT16_MAX);
  int32 r6 = ob_rand_int32 (INT32_MIN, INT32_MAX);
  sprintf (b, "%08X-%04X-%04X-%04X-%04X%08X", r1, r2, r3, r4, r5, r6);
  printf ("%s\n", b);
  return b;
}

Str DepositRequestOrCommand (const Protein &p)
{ ObRetort tort = hose_remote_data -> Deposit (p);
  std::cout << "tort is " << tort . Code () << "\n";
  Str guid = "";
  p . Ingests () . MapFind ("conversation-id") . Into (guid);
  return guid;
}

Protein TextfieldRequestProtein (const Str &recipient, const Str &hint, bool vibrate)
{ Str conversation_id = Str (CreateGUID ());
  if (hint != "")
    return Protein (Slaw::List ("remote-request-textfield", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", true, "hint", hint, "vibrate", vibrate,
          "conversation-id", conversation_id));
  else
    return Protein (Slaw::List ("remote-request-textfield", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", false, "hint", "you should not see this on device", "vibrate", vibrate,
          "conversation-id", conversation_id));
}

Protein ImageRequestProtein (const Str &recipient, const Str &hint,
      int max_width, int max_height, bool vibrate)
{ Str conversation_id = Str (CreateGUID ());
  if (hint != "")
    return Protein (Slaw::List ("remote-request-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", true, "hint", hint, "vibrate", vibrate, "max-width", max_width, "max-height", max_height,
          "conversation-id", conversation_id));
  else
    return Protein (Slaw::List ("remote-request-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
          Slaw::Map ("show-hint", false, "hint", "you should not see this on device", "vibrate", vibrate,
          "max-width", max_width, "max-height", max_height, "conversation-id", conversation_id));
}

Protein ShowSkinImageProtein (const Str &recipient, const Str &filename)
{ Str conversation_id = Str (CreateGUID ());
  return Protein (Slaw::List ("remote-show-skin-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                  Slaw::Map ("skin-name", filename,
                             "conversation-id", conversation_id));
}

Protein SaveSkinImageProtein (const Str &recipient, const Str &filename)
{ unsigned char *buffer = new unsigned char [1024 * 1024 * 10];  // 10 megabytes max
  int n = 0;
  FILE *f = fopen (filename, "rb");
  if (f)
    { n = fread (buffer, 1, 1024 * 1024 * 10, f); }
  else
    { printf ("SaveSkinImageProtein cannot open file\n"); }
  printf ("read %d bytes\n", n);
  return Protein (Slaw::List ("remote-save-skin-image", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("skin-name", filename, "skin-format", "image/jpeg", "skin-data", Slaw (buffer, n)));
  delete buffer;
}

Protein ShowSkinColorProtein (const Str &recipient, float r, float g, float b)
{ return Protein (Slaw::List ("remote-show-skin-color", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("skin-red", r, "skin-green", g, "skin-blue", b));
}

Protein DismissProtein (const Str &recipient, const Str &hint, bool vibrate)
{ Str conversation_id = Str (CreateGUID ());
  if (hint != "")
    return Protein (Slaw::List ("remote-dismiss", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("show-hint", true, "hint", hint, "vibrate", vibrate,
                "conversation-id", conversation_id));
  else
    return Protein (Slaw::List ("remote-dismiss", REMOTE_PROTEINS_FORMAT_VERSION, RecipientDescrip (recipient)),
                Slaw::Map ("show-hint", false, "hint", "you should not see this on device", "vibrate", vibrate,
                "conversation-id", conversation_id));
}

#endif  // REMOTE_UTILS_FOR_SELFISH_PLASMA
