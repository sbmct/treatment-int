
/* (c)  Oblong Industries */


#ifndef USEFUL_GELATIN
#define USEFUL_GELATIN

#include <libNoodoo/WindowedVisiFeld.h>
#include <libNoodoo/TexQuad.h>

using namespace oblong::noodoo;


/// to set transparency and color use:
///   gel -> SetBackingColor (ObColor (.0, 1.0, .0, .1));
class Gelatin  :  public TexQuad
{ PATELLA_SUBCLASS (Gelatin, TexQuad);

 private:
  Vect north, east;
  float32 texL;
  float32 texR;
  float32 texT;
  float32 texB;
  Vect v;
  float radius;
  void __inline__ SetEasyVertex (float64 over, float64 up);
 public:
  Gelatin ();
  double Radius ();
  void SetRadius (float64 r);
  void DrawSelf (VisiFeld *vf, Atmosphere *atm);

};

#endif  // USEFUL_GELATIN
