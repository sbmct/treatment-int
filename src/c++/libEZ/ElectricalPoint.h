
/* (c)  oblong industries */

#ifndef _ELECTRICAL_POINT_FOREVERMORE
#define _ELECTRICAL_POINT_FOREVERMORE


#include <libNoodoo/FlatThing.h>
#include <libNoodoo/VisiFeld.h>
#include <libNoodoo/SoftVertexForm.h>
#include <libNoodoo/VertEllipse.h>

#include <libImpetus/OEPointing.h>

#include <libBasement/SoftColor.h>
#include <libBasement/TWrangler.h>
#include <libBasement/SWrangler.h>
#include <libBasement/RWrangler.h>
#include <libBasement/CoordTransformWrangler.h>
#include <libBasement/ThrobWrangler.h>
#include <libBasement/AsympVect.h>
#include <libBasement/ob-hook-troves.h>

#include <libLoam/c++/ObMap.h>
#include <libLoam/c++/ObUniqueTrove.h>
#include <libLoam/c++/Str.h>
#include <libLoam/c++/Vect.h>

#include <libBasement/AsympFloat.h>

#include <libGanglia/ElectricalEvent.h>

namespace oblong {  
using namespace oblong::impetus;
using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::noodoo;
using namespace oblong::ganglia;

class ElectricalPoint  :  public OEPointing::Evts, public FlatThing
{ PATELLA_SUBCLASS (ElectricalPoint, FlatThing);

OB_PUBLIC:
  // this is just a workaround to let us pass around arrays of exactly
  // 12 Vects. Thanks, C++!
  struct ElectricalTine {
    Vect tines[12];
  };

  void InjectEvent (ElectricalEvent* evt, Atmosphere *atmo);

  /// A set of
  class ElectricalPointStyle  :  public AnkleObject
    { OB_PRIVATE:
       /// name used to look up this ElectricalPointStyle in ElectricalPoint's
       /// SetStyleByName.
       Str name;
       /// Sets of three quadrilaterals per array used in different
       /// ElectricalPoint states: hardened and softened. We separate two
       /// sets of points that get treated slightly differently by
       /// the ElectricalPoint: an alpha set, that can have their own
       /// color and bobble frequency, and a beta set, that march to
       /// the beat of a different drummer (err, that is, they can
       /// have a different color and bobble frequency from the alpha
       /// set. The tines describe points relative to the extents of
       /// the ElectricalPoint's size in our conventional yovo way
       /// (between -0.5 and 0.5). -0.5, -0.5 is the bottom left hand
       /// corner of the ElectricalPoint's "size" while 0.5, 0.5 is the top
       /// right. the z coordinate should generally be 0.0, unless you
       /// want a pointer mode where the shapes are out of the plane
       /// of the ElectricalPoint's flatness.
       ElectricalTine softened_alpha_tines, softened_beta_tines;
       ElectricalTine hardened_alpha_tines, hardened_beta_tines;
       ElectricalTine off_feld_tines;
       /// The ElectricalPoint applies the alpha_wrangler to the alpha tines,
       /// and the beta wrangler to the beta tines. These can be the
       /// same or different wranglers, or wranglers that don't really
       /// bobble at all, but they must not be NULL.
       ObRef <ThrobWrangler *> alpha_wrangler, beta_wrangler;

      public:
       ElectricalPointStyle (const Str &p_name,
                        Vect soft_a_tines[12], Vect soft_b_tines[12],
                        Vect hard_a_tines[12], Vect hard_b_tines[12],
                        Vect off_tines[12],
                        ThrobWrangler *a_throb, ThrobWrangler *b_throb);

       /// note that everything here that returns a reference
       /// returns const. we don't want people mucking with the internals
       /// of a ElectricalPointStyle after it's been created, since it's
       /// shared across multiple (many) ElectricalPoints.
        const Str &Name ()  const
          { return name; }
        const ElectricalTine SoftenedAlphaTines ()  const
          { return softened_alpha_tines; }
        const ElectricalTine SoftenedBetaTines ()  const
          { return softened_beta_tines; }
        const ElectricalTine HardenedAlphaTines ()  const
          { return hardened_alpha_tines; }
        const ElectricalTine HardenedBetaTines ()  const
          { return hardened_beta_tines; }
        const ThrobWrangler *AlphaWrangler ()  const
          { return ~alpha_wrangler; }
        const ThrobWrangler *BetaWrangler ()  const
          { return ~beta_wrangler; }
        const ElectricalTine OffFeldTines ()  const
          { return off_feld_tines; }
    };
 OB_PRIVATE:
  /// internal boolean indicating whether or not
  /// the handipoint appears on a feld
  bool onscreen;
  /// we set the ElectricalPoint's location by translating it to the
  /// appropriate place.
  ObRef <TWrangler *> child_loc_wrangler;
  ObRef <CoordTransformWrangler *> child_orientation_wrangler;
  /// we can also rotate the tines: typically reserved for offscreen
  /// graphics.
  ObRef <RWrangler *> rot_wrangler;

  /// last VisiFeld this ElectricalPoint appeared on.
  ObWeakRef <VisiFeld *> last_vf;
  /// alpha set in 0-2, beta set in 3-5
  ObTrove <SoftVertexForm *> tines;
  /// little circle that shows the handipoint user's identity (by color),
  /// if the flag ShouldShowProvenance is set to true.
  ObRef <VertEllipse *> provenance_dot;
  /// the two sets of handipoint tines can throb at different rates.
  ObRef <ThrobWrangler *> alpha_tine_throbber, beta_tine_throbber;

  /// scaler that stretches the underlying shapes out to the extents
  /// of the ElectricalPoint (we expect ElectricalPointStyles to generally
  /// have points with values between -0.5 and 0.5, so we scale them
  /// up here).
  ObRef <SWrangler *> tine_sizer;
  /// the style of the shapes and wobbliness of this handipoint.
  ObRef <ElectricalPointStyle *> hp_style;

  /// flag to turn on/off optional providence marker.
  bool should_show_provenance : 1;
  /// flag to turn on/off whether or not a reminder glyph appears
  /// at the edge of the closest feld when the ElectricalPoint is not
  /// actually on one of the felds.
  bool should_show_off_feld_marker : 1;
  /// hold on to whether or not this ElectricalPoint should be in its "softened"
  /// state (as opposed to "hardened")--we need this when the glyph is
  /// off feld, because we don't change to show hardened/softened then and
  /// need to know which state we're in when we're back on).
  bool am_softened : 1;
  /// determine whether or not a Move event should trigger relocating
  /// this handipoint.
  bool am_frozen : 1;
  /// flag for controlling whether or not this handipoint should be
  /// freed for other event sources when its adopted provenance disappears.
  bool am_exclusive : 1;
  /// keep track of whether or not this handipoint has condensed on screen.
  bool am_condensed : 1;

  /// provenance to handipoint map
  static ObMap <Str, ElectricalPoint *> use_map;
  /// list of validated and registered styles
  static ObMap <Str, ElectricalPointStyle *> style_map;
  /// list of all living ElectricalPoints
  static ObUniqueTrove <ElectricalPoint*, WeakRef> all_handipoint_trove;

  ObRetort SetStyle (ElectricalPointStyle *hps);

  Vect prev_handi_loc, cur_handi_loc;

OB_PUBLIC:
  /// look up ElectricalPoint for provenance. returns NULL if there
  /// is currently not a ElectricalPoint for the given provenance (prov).
  static ElectricalPoint *FindByProvenance (const Str &prov);

  /// returns a crawl of all living handipoints.
  static ObCrawl<ElectricalPoint *> AllElectricalPointCrawl ();

  /// load basic styles.
  static bool InitializeClassRidiculously ();

  /// validates that the given style can work and holds on to
  /// it in a static list of available modes. If the name of the style
  /// to add conflicts with a style already in the list,
  static ObRetort RegisterStyle (ElectricalPointStyle *hps);
  /// looks up the ElectricalPointStyle by name. returns NULL if there isn't
  /// one (or it hasn't been registered).
  static const ElectricalPointStyle *StyleByName (const Str &name);
  /// the default style is "point"
  static Str DefaultStyleName ();

  /// creates a handipoint with the default style.
  ElectricalPoint ();
  virtual ~ElectricalPoint ();

  ObRetort SetStyleByName (const Str &name);
  Str StyleName ()  const;


  const Str ElectricalProvenance (OEPointingEvent *e) const;

  /// apply colors for the given pointing source (as a provenance
  /// string prv).
  void StyleForProvenance (const Str &prv);

  /// control over whether or not markers should be drawn on the edge
  /// of the closest feld when the pointing source does not intersect
  // with any felds.
  void SetShouldShowOffFeldMarker (bool truth)
    { if (should_show_off_feld_marker  ==  truth)
        return;
      should_show_off_feld_marker = truth;
    }
  bool QueryShouldShowOffFeldMarker ()
    { return should_show_off_feld_marker; }

  /// control over whether or not the provenance / ownership indicator
  /// is drawn.
  void SetShouldShowProvenance (bool truth)
    { if (~provenance_dot)
        (~provenance_dot) -> SetShouldDraw (truth);
      should_show_provenance = truth;
    }
  bool QueryShouldShowProvenance ()
    { return should_show_provenance; }

  /// control over whether or not the handipoint should be available
  /// to multiple event sources after its event source disappears.
  void SetIsExclusive (bool truth)
    { am_exclusive = truth; }
  bool QueryIsExclusive ()  const
    { return am_exclusive; }

  void SetRotation (float64 a);

  virtual ObRetort OEPointingAppear (OEPointingAppearEvent *e,
                                     Atmosphere *atm);
  virtual ObRetort OEPointingVanish (OEPointingVanishEvent *e,
                                     Atmosphere *atm);
  virtual ObRetort OEPointingMove (OEPointingMoveEvent *e,
                                   Atmosphere *atm);
  virtual ObRetort OEPointingSoften (OEPointingSoftenEvent *e,
                                     Atmosphere *atm);
  virtual ObRetort OEPointingHarden (OEPointingHardenEvent *e,
                                     Atmosphere *atm);
  virtual ObRetort OEPointingLinger (OEPointingLingerEvent *e,
                                     Atmosphere *atm);

  /// force the handipoint to accept events from the given provenance.
  /// if this handipoint is not exclusive, its OEPointing event handlers
  /// will automatically adopt an unknown provenance.
  ObRetort AdoptProvenance (const Str &prv);

  /// if this handipoint is not exclusive, it calling this method will
  /// free the handipoint to listen to another pointing source.
  /// note that all the default OEPointing handlers call this method
  /// when needed.
  ObRetort DisownProvenance ();

  /// returns whether or not this handipoint can accept OEPointing events
  /// for the given source. if this handipoint has already adopted a
  /// particular provenance amd that provenance matches the specified prv,
  /// this method returns OB_OK. If the ElectricalPoint is not exclusive and
  /// is not currently serving any provenance, it will adopt prv and also
  /// return OB_OK. If it cannot serve the given provenance, it returns
  /// OB_EVENT_UNMATCHED_PROVENANCE. Note that all OEPointing event handlers
  /// call this method automatically; it is unlikely to be needed outside of
  /// but provided to the API user in case they want to customize ElectricalPoint
  /// allocation.
  ObRetort ValidateEventProvenance (const Str &prv);

  /// if the handipoint's event source does not intersect with any feld,
  /// this method can be used to find a point (loc) on the edge of the closest
  /// feld and a direction (direction) that approximates where the handipoint
  /// is off the feld
  virtual bool FindFeldEdge (const Vect &origin, const Vect &aimer,
                             Vect &loc, Vect &direction, float64 &angle);

  /// update internal state to scale glyphs to the appropriate size.
  virtual ObRetort AcknowledgeSizeChange ();
  /// update internal state to orient glyphs appropriately
  virtual ObRetort AcknowledgeOrientationChange ();

  /// soft fade-in; called upon OEPointingAppear.
  virtual void Condense ();
  /// soft fade-out; called upon OEPointingVanish.
  virtual void Evaporate ();

  /// stop tracking the location of the provenance
  virtual void Freeze ();
  bool Frozen ();
  /// begin tracking again
  virtual void Thaw ();

  /// update visual effects to show that the pointing gesture
  /// has hardened.
  virtual void Harden ();
  /// update visual effects to show that the pointing gesture
  /// has softened.
  virtual void Soften ();

  /// update visual effects to show that pointing gesture is offscreen
  void ConvertToOffscreen ();
  /// update visual effects to show that pointing gesture returns onscreen
  void ConvertToOnscreen ();

};

} // a bittersweet farewell to namespace oblong

#endif // _ELECTRICAL_POINT_FOREVERMORE
