
/* (c)  oblong industries */


#ifndef BASIC_WAND_ROLL_GLIMPSER
#define BASIC_WAND_ROLL_GLIMPSER

// continuous roll angle ; generates OERatchetProgress events
// does NOT generate OERatchetSnick events

#include <libAfferent/Glimpser.h>
#include <libImpetus/OERatchet.h>
#include <libImpetus/OEPointing.h>
#include <libAfferent/AfferentRetorts.h>
#include <libBasement/ob-logging-macros.h>

using namespace oblong::afferent;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;

class BasicWandRollGlimpser  :  public Glimpser,
                                public OEPointing::Evts
{ PATELLA_SUBCLASS (BasicWandRollGlimpser, Glimpser);

 OB_PRIVATE:
  FLAG_MACHINERY_FOR (InterpretPointingEvents);
  FLAG_MACHINERY_FOR (InterpretDisplacementEvents);
  FLAG_MACHINERY_FOR (GenerateProgressEvents);

  struct Papoose {
    Str provenance;
    Vect orig_axis;
    Str cur_species;
    Vect prev_palm;
    Vect prev_aim;
    float64 cumu_angle;
    float64 orig_tstamp, prev_tstamp;
    int32 refractory;
    FatherTime uhr;
    Papoose (const Str &prov, const Vect &palm, float64 ptst);
  };

  ObTrove <Papoose *> pooses;
  bool justHarden;
  double tableHeight;

 public:
  BasicWandRollGlimpser ();

  Papoose *PapooseByProvenance (const Str &prov);

  OERatchetProgressEvent *GinUpProgressEvent (Papoose *pap, const Str &genus);

  void ConsiderTrajectory (Papoose *p, OEPointingMoveEvent *e, Vect palm, float64 tstmp,
                           Atmosphere *atm, Vect *aim = NULL);

  ObRetort OEPointingHarden (OEPointingHardenEvent *e, Atmosphere *atm);
  ObRetort OEPointingAppear (OEPointingAppearEvent *e, Atmosphere *atm);
  ObRetort OEPointingVanish (OEPointingVanishEvent *e, Atmosphere *atm);
  ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm);

  void SetTableHeight (double ta)
    { tableHeight = ta;
    }

};

#endif