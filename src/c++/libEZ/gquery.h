
/* (c) oblong industries */

#pragma once

#ifndef g_QUERY_H
#define g_QUERY_H

#include <libBasement/KneeObject.h>
#include <libNoodoo/VisiDrome.h>
#include <set>
#include <vector>
#include <deque>

using namespace oblong::noodoo;

///  gquery: jQuery selectors lite, for g-speak
///
///  author: Brandon Harvey <brandon@oblong.com>
///
///  :::::::::::::gquery basics::::::::::::::::::::::::::::::::::::::
///
///  Analogous to jQuery selectors, gquery is a function that selects
///  KneeObjects out of a Drome (or any other NestyThing) hierarchy,
///  using a compact query expression.
///
///  In jQuery,
///            $("#foo") selects the object with id == "foo".
///            $("h2") selects all objects of the 'h2' type.
///            $(".bar") selects all objects with class attribute "bar".
///
///  In gquery,
///            g$ ("foo") selects the object with Name () == "foo".
///            g$ <Baz> ("*") selects all objects of the Baz type
///            g$ (".bar") selects all objects tagged with attribute "bar".
///
///  There are 3 basic flavors of g$ function:
///     g$  returns a vector of pointers to matching objects.  Vector may be empty.
///     g$1 returns only a pointer to the first match.  Pointer may be NULL.
///     g$t returns an ObTrove rather than a vector.  Trove may be empty.
///
///  :::::::::::::query syntax::::::::::::::::::::::::::::::::::::::::
///
///  ==> "*" selects everything (so does "")
///
///  ==> "query" queries by name (as in, KneeObject Name ())
///
///          Example: KneeObject A;
///                   A . SetName ("foobar");
///                   g$ ("foobar")           // selects A
///
///  ==> ".query" queries against the whatnot Str tags attached to a KneeObject
///          (Whatnot is a real thing!  Check out KneeObject.h)
///
///          Example: A . AppendWhatnot ("bar", Slaw::Null ());
///                   NestyThing B;
///                   B . SetName ("bart");
///                   B . AppendWhatnot ("bar", Slaw::Null ());
///                   FlatThing C;
///                   C . AppendWhatnot ("baz", Slaw::Null ());
///                   g$ (".bar")       // selects both A and B
///
///  ==> To query by type, using a template argument. Selects by exact OR inherited class.
///
///          Example: g$ <NestyThing> ("*")  // selects both B and C, above,
///                                          // since FlatThings are NestyThings
///
///      This query is ANDed with other queries.  Example:
///
///         //  Grabs one object, type is GlypoString AND name is help-string1
///         GlyphoString *helpStr1 = g$1 <GlyphoString> ("help-string1"))
///
///  ==> Queries can have wildcards ? and * in them
///
///          Example: g$ (".ba?") selects A, B, and C above, by whatnot
///          Example: g$ ("*ba*") selects A and B above, by name
///
///  ==> If an object appears more than once in the drome's scenegraph,
///      it will only appear in the results once.
///
///  ==> Nesting is possible, chaining is not (yet):
///
///      //  Grab all objects on the windshield with a 'circle' tag in their whatnot.
///      //  The expression 'g$1 <Windshield> ("*")' seeks any Windshield.
///      //  If one is found, the query for circle will search within that Windshield.
///      //  Otherwise, the query will use drome-wide scope.
///      vector <KneeObject *> = g$ (".circle", g$1 <Windshield> ("*")))
///
///  ==> gquery strongly suggests that the Name () of KneeObjects be unique!
///
///  ==> gquery does a breadth-first search of drome's graph.  Objects higher
///      graph will be found sooner, and be listed earlier in results.


// For reference: the Whatnot API for KneeObjects
//    ObRetort AppendWhatnot (const Str &tag, const Slaw &whatnot);
//    const Slaw &FindWhatnot (const Str &tag)  const;
//    int64 WhatnotCount ()  const;
//    const Slaw &NthWhatnot (int64 ind)  const;
//    const Str &NthWhatnotTag (int64 ind)  const;
//    ObRetort RemoveWhatnot (const Str &tag);
//    ObRetort RemoveNthWhatnot (int64 ind);


namespace oblong { namespace gquery {


//  adapted from http://www.codeproject.com/Articles/1088/Wildcard-string-compare-globbing
inline bool wildcardMatch (const char *text, const char *pattern)
{ const char *cp = NULL, *mp = NULL;

  //  Read up until first * in pattern
  while ((*text) && (*pattern != '*'))
    { if ((*pattern != *text) && (*pattern != '?'))
        return false;
      pattern++;
      text++;
    }

  while (*text)
    { if (*pattern == '*')
        { if (!*++pattern)  // at end of the pattern
            return true;
          mp = pattern;
          cp = text+1;
        }
      else if ((*pattern == *text) || (*pattern == '?'))
        { pattern++;
          text++;
        }
      else
        { pattern = mp;
          text = cp++;
        }
    }

  while (*pattern == '*')
    pattern++;

  return !*pattern;
}


inline bool HasWhatnotTag (const KneeObject &k, const char *query)
{ for (int64 n = 0 ; n < k . WhatnotCount () ; n++)
    if (wildcardMatch (k . NthWhatnotTag (n) . utf8 (), query))
      return true;

  return false;
}


inline bool MatchesNameQuery (const KneeObject &k, const char *query)
{ if (k . Name () . IsEmpty ()) //  Objects with no name don't match
    return false;

  return wildcardMatch (k . Name () . utf8 (), query);
}


inline bool KneeObjectMatches (const KneeObject &k, const char *query, bool name_match)
{ if (name_match)  return MatchesNameQuery (k, query);
  else             return HasWhatnotTag    (k, query);
}

///  Breadth-first search, so that objects higher in the scene graph are found sooner
///  For fastest perf, all-matching query should be passed as '*', not ''
template <typename T> inline std::vector <T*> g$walk (NestyThing *parent,
                                                      const char *query,
                                                      bool stop_on_first_match = false,
                                                      bool name_match = true,
                                                      bool match_any = false)
{ std::vector <T*> results;
  if (! parent) return results;

  //  Set prevents inspecting (or, worse, descending into) anything twice.
  std::set   <KneeObject*>  visitedYet;
  std::deque <NestyThing *> q;
  NestyThing *childAsNesty = NULL;
  q . push_front (parent);

  while (! q . empty ())
    { NestyThing *n = q . back ();
      q . pop_back ();

      // OB_LOG_INFO ("g$ visiting nesty %s, name: %s", n -> ClassName (),
      //               n -> Name () . utf8 ());

      for (int64 i = 0 ; i < n -> ChildCount () ; i++)
        { KneeObject *knee = n -> NthChild (i);
          if (visitedYet . find (knee) != visitedYet . end ())
            continue;
          visitedYet . insert (knee);
          T* castedChild = dynamic_cast <T*> (knee);

          // OB_LOG_INFO ("  g$ (%s) visiting child %d, class %s, name: [%s], %s",
          //               quer . utf8 (), (int)i, knee -> ClassName (),
          //               knee -> Name () . utf8 (), castedChild ? "casts" : "doesn't cast");

          if (castedChild &&
              (match_any || KneeObjectMatches (*knee, query, name_match)))
            { results  . push_back (castedChild);
              if (stop_on_first_match)
                return results;
            }

          // Queue up a visit into this child, if it's nesty
          if ((childAsNesty = dynamic_cast <NestyThing*> (knee)))
            q . push_front (childAsNesty);
        }
    }
  return results;
}

///  Pre-processes the query and calls g$walk
template <typename T> inline std::vector <T*> g$core (NestyThing *parent,
                                                      const Str &query,
                                                      bool stop_on_first_match = false)
{ Str quer = query;
  quer . Strip ();
  if (quer . IsEmpty ())
    quer = "*";  // empty queries are equivalent to "*"
  UChar32 initial = quer . At (0);

  bool matchesAll = (quer == "*" && quer . Length () == 1);
  bool nameMatch  = (initial != '.');   // Anything other than '.' is match-by-name.

  if (! nameMatch)
    { quer = quer . Slice (1);          // Cut off the leading specifier.
      if (quer . Length () == 0)
        return std::vector <T*> ();     // Nothing left after lead char?  Bail.
    }
  char *queryAsChars = strdup (quer . utf8 ());

  return g$walk <T> (parent, queryAsChars, stop_on_first_match, nameMatch, matchesAll);
}


// ------------------------------------------------ g$
//

///  Call as
///    vector <KneeObject *> tr = g$ ("foo");    // Search scope is the drome.
///  OR
///    vector <KneeObject *> tr = g$ ("foo", nesty);
///  Results vector may be empty.
inline std::vector <KneeObject *> g$ (const Str &query,
                                      NestyThing *n = UrDrome::OutermostDrome ())
{ return g$core <KneeObject> (n, query);
}


///  Call as
///    vector <SomeObject *> tr = g$ <SomeObject> ("foo");  // Search scope is the drome.
///  OR
///    vector <SomeObject *> tr = g$ <SomeObject> ("foo", nesty);
///  Results vector may be empty.
template <typename T> inline std::vector <T *> g$ (const Str &query,
                                                   NestyThing *n = UrDrome::OutermostDrome ())
{ return g$core <T> (n, query);
}


// ------------------------------------------------ g$t
//

///  Call as
///    ObTrove <KneeObject *> tr = g$t ("foo");   // Search scope is the drome.
///  OR
///    ObTrove <KneeObject *> tr = g$t ("foo", nesty);
///  Results trove may be empty.
inline ObTrove <KneeObject *> g$t (const Str &query,
                                   NestyThing *n = UrDrome::OutermostDrome ())
{ std::vector <KneeObject *> results = g$core <KneeObject> (n, query);
  ObTrove <KneeObject *> output (&results[0], results . size ());
  return output;
}


///  Call as
///    ObTrove <SomeObject *> tr = g$t <SomeObject> ("foo");   // Search scope is the drome.
///  OR
///    ObTrove <SomeObject *> tr = g$t <SomeObject> ("foo", nesty);
///  Results trove may be empty.
template <typename T> inline ObTrove <T *> g$t (const Str &query,
                                                NestyThing *n = UrDrome::OutermostDrome ())
{ std::vector <T*> results = g$core <T> (n, query);
  ObTrove <T*> output (&results[0], results . size ());
  return output;
}



// ------------------------------------------------ g$1 (select just a single item)
//

///  Call as
///    KneeObject *o = g$1 ("foo");               // Search scope is the drome.
///  OR
///    KneeObject *o = g$1 ("foo", nesty);
//   Result pointer o may be NULL
inline KneeObject * g$1 (const Str &query,
                         NestyThing *n = UrDrome::OutermostDrome ())
{ std::vector <KneeObject*> results = g$core <KneeObject> (n, query, true);
  if (results . size () == 0) return NULL;
  else                        return results[0];
}


///  Call as
///    SomeObject *o = g$1 <SomeObject> ("foo");   // Search scope is the drome.
///  OR
///    SomeObject *o = g$1 <SomeObject> ("foo", nesty);
//   Result pointer o may be NULL
template <typename T> inline T* g$1 (const Str &query,
                                     NestyThing *n = UrDrome::OutermostDrome ())
{ std::vector <T*> results = g$core <T> (n, query, true);
  if (results . size () == 0) return NULL;
  else                        return results[0];
}

/// added by Alex Valli: this is just a plain renaming of the method above
template <typename T> inline T* Find (const Str &query,
                                     NestyThing *n = UrDrome::OutermostDrome ())
{ std::vector <T*> results = g$core <T> (n, query, true);
  if (results . size () == 0) return NULL;
  else                        return results[0];
}




} } // buh bye gquery, oblong namespaces

#endif

