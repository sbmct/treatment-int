
/* (c)  Oblong Industries */


#ifndef REMOTE_FOR_MANY_COOL_DEVICES
#define REMOTE_FOR_MANY_COOL_DEVICES

#include <libLoam/c++/Str.h>
#include <libBasement/ob-logging-macros.h>
#include <libBasement/SpaceFeld.h>
#include <libNoodoo/VisiFeld.h>
#include <libAfferent/Glimpser.h>
#include <libImpetus/OERatchet.h>
#include <libImpetus/OEDisplacement.h>
#include <libImpetus/OEBlurt.h>
#include <libBasement/RWrangler.h>
#include <libBasement/ImageClot.h>
#include <libMedia/JpegImageClot.h>

using namespace oblong::noodoo;
using namespace oblong::afferent;
using namespace oblong::loam;
using namespace oblong::media;

/** This enum defines the three possible pointing states.  */
typedef enum
  { NoTouch,     /** The user is not touching the display.  */
    Touch,       /** The user is touching the display.  */
    TapAndTouch  /** The user is hardening.  */
  } TouchType;

/** This enum defines the nature of the received pointing updates.
 *  Simply it is kRelative for iOS devices, and kAbsolute in js.  */
typedef enum
  { Relative,    /** Pointing updates are relative (iOS, Android).  */
    Absolute     /** Pointing updates are absolute (js, wiimotes).  */
  } ModeType;

#define NO_SWIPE_UTTERANCE      "no swipe"
#define SWIPE_UP_UTTERANCE      "swipe up"
#define SWIPE_RIGHT_UTTERANCE   "swipe right"
#define SWIPE_DOWN_UTTERANCE    "swipe down"
#define SWIPE_LEFT_UTTERANCE    "swipe left"

/** Utterances for the discrete swipe blurts. We might want to change the strings.  */
static Str SwipeUtterances[5] =
  { Str (NO_SWIPE_UTTERANCE),      /** This should never be used.  */
    Str (SWIPE_UP_UTTERANCE),      /** Discrete swipe up.  */
    Str (SWIPE_RIGHT_UTTERANCE),   /** Discrete swipe right.  */
    Str (SWIPE_DOWN_UTTERANCE),    /** Discrete swipe down.  */
    Str (SWIPE_LEFT_UTTERANCE)     /** Discrete swipe left.  */
  };

/**  Maximum number of remote controllers that can be tracked.  */
#define MAX_TRACKERS 8
/**  Version string for the proteins format.  */
#define REMOTE_PROTEINS_FORMAT_VERSION "version.2.0"
/**  Name of the pool used to exchange messages with the mobile device or web interface.  */
#define REMOTE_POOL_NAME    "remote"
//  pool used to receive uploaded images and other input from the mobile device
#define REMOTE_DATA_POOL_NAME   "remote-data"
/**  Time before a pointing tracker entity dies completely.  */
#define MAX_LIFE        1200.0
/**  Time before a pointing tracker entity vanishes.  */
#define VANISH_LIFE     15.0
/**  Time before a pointing tracker entity is recovered from out-of-feld space.  */
#define RECOVERY_LIFE    1.0


/**
 *  This class enables any g-speak application to receive input from an iOS device
 *  or javascript (js-plasma) browser controller. Also we recently implemented
 *  an Android app and Wiimotes support.
 */
class Remote  :  public NestyThing
{ PATELLA_SUBCLASS (Remote, NestyThing);

 OB_PRIVATE:

  class Tracker  :  public LocusThing
    { PATELLA_SUBCLASS (Tracker, LocusThing);
     public:
      FatherTime *life_timer;
      Vect angles, prev_angles, last_in_feld_angles;
      Vect aim, prev_aim, last_in_feld_aim;
      int touch;
      bool prev_tap_and_touch;
      int number_of_touches;  // number of fingers on the display
      Str provenance;
      Str name;
      Str version;
      Str model;
      Str unique_identifier;
      Str interaction_mode;
      Str os;
      int ratchet_state;
      double angle;
      Remote *remote;
      RWrangler *horizontal;
      RWrangler *vertical;
      double normal_displacement;
      bool lost;
      bool vanished;
      Tracker (Remote *rr);
      virtual ~Tracker ();
      virtual ObRetort Travail (Atmosphere *atm);
    };

  Glimpser *internal_glimpser;
  static Remote* instance;  //  the only instance of this Singleton
  SpaceFeld *rsf;  //  reference SpaceFeld
  Vect origin;
  double sensitivity;
  /**  This constructor is not used directly. Use Remote::Instance () instead.  */
  Remote (const Str &hostname_with_pools, SpaceFeld *referenceSpaceFeld);
  bool BroadcastPointingMoveEvent (const Str &provenance, Vect aim, Vect prev,
        double normalDisplacement, Atmosphere *atm);
  void BroadcastDisplacementMoveEvent (const Str &provenance, Vect displ, Atmosphere *atm);
  void BroadcastPointingVanishEvent (const Str &provenance,
        Vect aim, Atmosphere *atm);
  void BroadcastPointingAppearEvent (const Str &provenance,
        Vect aim, Atmosphere *atm);
  void BroadcastPointingHardenEvent (const Str &provenance,
        Vect aim, Atmosphere *atm);
  void BroadcastPointingSoftenEvent (const Str &provenance,
        Vect aim, Atmosphere *atm);
  void BroadcastRatchetProgressEvent (const Str &provenance,
        float64 angle, Atmosphere *atm);
  void BroadcastRatchetSnickEvent (const Str &provenance,
        const Str &direction, Atmosphere *atm);
  void BroadcastBlurtAppearEvent (const Str &provenance,
        const Str &utterance, Atmosphere *atm);
  //  this is called every time a new protein is deposited in the pool
  //  (i.e. mobile device sensors' updates in our present case)
  ObRetort MetabolizePointing (const Protein &prt, Atmosphere *atm);
  ObRetort MetabolizeHello (const Protein &prt, Atmosphere *atm);

 public:
  virtual ~Remote ();

  static Str full_remote_pool_name;
  static Str full_remote_data_pool_name;
  
  bool BroadcastPointingMoveEvent (const Str &provenance, Vect orig, Vect thro,
        Atmosphere *atm);

  /**
   *  This class enables any subclassing class to metabolize default remote proteins.
   */
  class Metabolizer
    {
     public:
      virtual ObRetort Hello (const Str&provenance, const Str &name,
            const Protein &prt, Atmosphere *atm);
      virtual ObRetort Response (const Str &provenance, const Str &response,
            const Str &conversation_id, const Protein &prt, Atmosphere *atm);
      virtual ObRetort ImageUpload (const Str &provenance,
            const Str &conversation_id, const Protein &prt, Atmosphere *atm);
      virtual ObRetort TextfieldEdit (const Str &provenance, const Str &text,
            const Str &conversation_id, const Protein &prt, Atmosphere *atm);
      virtual ObRetort Textfield (const Str &provenance, const Str &text,
            const Str &conversation_id, const Protein &prt, Atmosphere *atm);
      virtual ObRetort ImageUploadAnnouncement (const Str &provenance, int width, int height,
            const Str &conversation_id, const Protein &prt, Atmosphere *atm);
     OB_PROTECTED:
      virtual ObRetort MetabolizeHello (const Protein &prt, Atmosphere *atm);
      virtual ObRetort MetabolizeResponse (const Protein &prt, Atmosphere *atm);
      virtual ObRetort MetabolizeImageUpload (const Protein &prt, Atmosphere *atm);
      virtual ObRetort MetabolizeImageUploadAnnouncement (const Protein &prt,
            Atmosphere *atm);
      virtual ObRetort MetabolizeTextfieldEdit (const Protein &prt, Atmosphere *atm);
      virtual ObRetort MetabolizeTextfield (const Protein &prt, Atmosphere *atm);
    };

  /** Sets the pointing tracker sensitivity, in range 0.1 - 10.0 (1.0 by default).
   *  It is defined as the ratio between pointing tracker speed and actual
   *  input controller motion.
   *  The parameter s is the new sensitivity value to be applied.
   */
  void SetSensitivity (double s);
  /** Returns the current sensitivity, that defaults to 1.0, and is in range 0.1 - 10.0  */
  double Sensitivity ();
  /** This class is a singleton. This method allows access to the only instance.
   *  The pointing tracker is initialized to aim at the center of the optional
   *  reference SpaceFeld parameter (default is the first VisiFeld). Also, an
   *  optional hostname can be specified, so that the remote pools can live on a 
   *  different machine.
   */
  static Remote* Instance (const Str &hostname_with_pools = "", SpaceFeld *referenceSpaceFeld = NULL);
  /** Returns the internal glimpser.
   */
  Glimpser *InternalGlimpser ();
  /** This static method creates a new ImageClot of the right size and
   *  fills it with the image contained in the protein.
   *  Formats supported are currently image/jpeg and RAW_RGBA.
   */
  static ImageClot *ExtractImageFromProtein (const Protein &prt);
  /**
   *  Checks provenance.
   */
  static bool SupportsAdvancedFunctions (OEPointingEvent *e);
  /**
   *  Creates and returns a recipient descrip.
   */
  static Str RecipientDescrip (const Str &recipient);
  /**
   *  Creates and returns an image request protein.
   */
  static Protein ImageRequestProtein (const Str &recipient, const Str &hint = "", int max_width = 800, int max_height = 800, bool vibrate = true);
  /**
   *  Creates and returns a show skin image command protein.
   */
  static Protein ShowSkinImageProtein (const Str &recipient, const Str &filename);
  /**
   *  Deposits a show skin image command.
   */
  static void DepositShowSkinImage (const Protein &p);
  /**
   *  Creates and returns a save skin image command protein.
   */
  static Protein SaveSkinImageProtein (const Str &recipient, const Str &filename);
  /**
   *  Deposits a save skin image command.
   */
  static void DepositSaveSkinImage (const Protein &p);
  /**
   *  Creates and returns a show skin color command protein.
   */
  static Protein ShowSkinColorProtein (const Str &recipient, ObColor c);
  /**
   *  Deposits a show skin color command.
   */
  static void DepositShowSkinColor (const Protein &p);
  /**
   *  Creates and returns a dismiss command protein.
   */
  static Protein DismissProtein (const Str &recipient, const Str &hint = "",  bool vibrate = false);
  /**
   *  Deposits a dismiss command.
   */
  static Str DepositDismiss (const Protein &p);
  /**
   *  Creates and returns a text field input request protein.
   */
  static Protein TextfieldRequestProtein (const Str &recipient, const Str &hint = "", bool vibrate = true);
  /**
   *  Deposits a textfield input request.
   */
  static Str DepositTextfieldRequest (const Protein &p);
  /**
   *  Deposits an image request.
   */
  static Str DepositImageRequest (const Protein &p);
  /**
   *  Extracts the recipient from the protein descrips.
   */
  static Str ExtractRecipientFromProtein (const Protein &p);
  /**
   *  Extracts the version from the protein descrips.
   */
  static Str ExtractVersionFromProtein (const Protein &p);
  /**
   *  Appends an event target.
   */
  ObRetort AppendEventTarget (KneeObject *ko);
  /**
   *  Finds or assigns a tracker with that provenance.
   */
  Tracker *FindTracker (const Str &provenance, Atmosphere *atm = NULL);
  /**
   *  Specifies the instance/class that will metabolize the incoming proteins.
   */
  template <typename KLASS> static void AppendDefaultMetabolizers (KneeObject *ko)
    { UrDrome::OutermostDrome () -> PoolParticipate (full_remote_data_pool_name, ko);
      ko -> AppendMetabolizer (Slaw::List ("remote-hello"),
            &KLASS::MetabolizeHello, "remote-hello");
      ko -> AppendMetabolizer (Slaw::List ("remote-response"),
            &KLASS::MetabolizeResponse, "remote-response");
      ko -> AppendMetabolizer (Slaw::List ("remote-textfield-edit"),
            &KLASS::MetabolizeTextfieldEdit, "remote-textfield-edit");
      ko -> AppendMetabolizer (Slaw::List ("remote-textfield"),
            &KLASS::MetabolizeTextfield, "remote-textfield");
      ko -> AppendMetabolizer (Slaw::List ("remote-image-upload"),
            &KLASS::MetabolizeImageUpload, "remote-image-upload");
      ko -> AppendMetabolizer (Slaw::List ("remote-image-upload-announcement"),
            &KLASS::MetabolizeImageUploadAnnouncement, "remote-image-upload-announcement");
    }
};

#endif  // REMOTE_FOR_IOS_AND_JS
