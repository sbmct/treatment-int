

/* (c)  oblong industries */

#ifndef ELECTRICAL_HANDIPOINT_EVENT
#define ELECTRICAL_HANDIPOINT_EVENT


#include <libImpetus/OEPointing.h>

#include <libGanglia/ElectricalEvent.h>


namespace oblong {  
using namespace oblong::ganglia;
using namespace oblong::impetus;
using namespace oblong::basement;
using namespace oblong::plasma;
using namespace oblong::loam;


class EEPointEvent  :  public ElectricalEvent
{ PATELLA_SUBCLASS (EEPointEvent, ElectricalEvent);
  OE_MISCY_MAKER (EEPoint, Electrical, "point");
OB_PROTECTED:
  Vect handi_point_loc, prev_handi_point_loc;
  Str intent;

OB_PUBLIC:
  EEPointEvent (KneeObject *orig_ko = NULL)  :  ElectricalEvent (orig_ko)
    { }

  const Vect &Location ()  const;
  const Vect &PreviousLocation ()  const;

  void SetCurrentLocation (const Vect &loc);
  void SetPreviousLocation (const Vect &loc);

  const Str &Intent ()  const;
  void SetIntent (const Str &ent);

  OEPointingEvent *RawOEPointingEvent ()  const;
  void SetRawOEPointingEvent (OEPointingEvent *pe);

  virtual void SynthesizeInternalProtein ();
  virtual ObRetort AnalyzeInternalProtein ();
};

class EEPointCondenseEvent  :  public EEPointEvent
{ PATELLA_SUBCLASS (EEPointCondenseEvent, EEPointEvent);
  OE_MISCY_MAKER (EEPointCondense, EEPoint, "condense");
  EEPointCondenseEvent (KneeObject *orig_ko = NULL)  :  EEPointEvent (orig_ko)
    { }
};

class EEPointEvaporateEvent  :  public EEPointEvent
{ PATELLA_SUBCLASS (EEPointEvaporateEvent, EEPointEvent);
  OE_MISCY_MAKER (EEPointEvaporate, EEPoint, "evaporate");
  EEPointEvaporateEvent (KneeObject *orig_ko = NULL)  :  EEPointEvent (orig_ko)
    { }
};

class EEPointMoveEvent  :  public EEPointEvent
{ PATELLA_SUBCLASS (EEPointMoveEvent, EEPointEvent);
  OE_MISCY_MAKER (EEPointMove, EEPoint, "move");
  EEPointMoveEvent (KneeObject *orig_ko = NULL)  :  EEPointEvent (orig_ko)
    { }
};

class EEPointHardenEvent  :  public EEPointEvent
{ PATELLA_SUBCLASS (EEPointHardenEvent, EEPointEvent);
  OE_MISCY_MAKER (EEPointHarden, EEPoint, "harden");
  EEPointHardenEvent (KneeObject *orig_ko = NULL)  :  EEPointEvent (orig_ko)
    { }
};

class EEPointSoftenEvent  :  public EEPointEvent
{ PATELLA_SUBCLASS (EEPointSoftenEvent, EEPointEvent);
  OE_MISCY_MAKER (EEPointSoften, EEPoint, "soften");
  EEPointSoftenEvent (KneeObject *orig_ko = NULL)  :  EEPointEvent (orig_ko)
    { }
};

class EEPointEventAcceptorGroup
 :     public  EEPointCondenseEvent :: EEPointCondenseAcceptor,
       public EEPointEvaporateEvent :: EEPointEvaporateAcceptor,
       public      EEPointMoveEvent :: EEPointMoveAcceptor,
       public    EEPointHardenEvent :: EEPointHardenAcceptor,
       public    EEPointSoftenEvent :: EEPointSoftenAcceptor
{ };


class EEPoint
{ public:
  typedef EEPointEventAcceptorGroup Evts;
};


} // a bittersweet end for namespace and oblong


DECLARE_INTERFACE_AS_ANKLE_OBJECT_SPECIALIZED
    (oblong::EEPoint::Evts);


#endif
