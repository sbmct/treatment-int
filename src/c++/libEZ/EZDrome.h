
/* (c)  Oblong Industries */


#ifndef EZDROME_FOR_DUMMIES
#define EZDROME_FOR_DUMMIES

//  let's include the remote pointing receiver code
#include "Remote.h"

//  wand rolling support
#include "BasicWandRollGlimpser.h"

// let's include some useful g-speak stuff - of course you can add more stuff
//  here in order not to clutter your source code file
#include <libLoam/c/ob-rand.h>
#include <libLoam/c++/ArgParse.h>
#include <libLoam/c++/ObTrove.h>
#include <libLoam/c++/ObMap.h>
#include <libBasement/ob-logging-macros.h>
#include <libBasement/KneeObject.h>
#include <libBasement/AsympFloat.h>
#include <libBasement/AsympVect.h>
#include <libBasement/QuadraticVect.h>
#include <libBasement/LinearColor.h>
#include <libBasement/AsympColor.h>
#include <libBasement/LogLogger.h>
#include <libBasement/SineFloat.h>
#include <libBasement/SineColor.h>
#include <libBasement/SineVect.h>
#include <libBasement/TWrangler.h>
#include <libBasement/SWrangler.h>
#include <libBasement/RWrangler.h>
#include <libBasement/TimedTrigger.h>
#include <libNoodoo/GLMiscellany.h>
#include <libNoodoo/WindowedVisiFeld.h>
#include <libNoodoo/VisiDrome.h>
#include <libNoodoo/ObStyle.h>
#include <libNoodoo/TexQuad.h>
#include <libNoodoo/VidQuad.h>
#include <libNoodoo/GlyphoString.h>
#include <libImpetus/OEComprehensive.h>
#include <libImpetus/OEBlurt.h>
#include <libImpetus/OEPointing.h>
#include <libGestation/MouseGTot.h>
#include <libGestation/KeyboardGTot.h>
#include <libTwillig/HandiPoint.h>
#include <libAfferent/Gestator.h>
#include <libAfferent/DualCase.h>
#include <libAfferent/RoddedGlimpser.h>
#include <libAfferent/OneHandPointingRod.h>
#include <libAfferent/WandPointingRod.h>
#include <libAfferent/OneHandDisplacementRod.h>
#include <libAfferent/OneHandBlurtRod.h>
#include <libPlasma/zeroconf/PoolServer.h>
#include <libPlasma/zeroconf/Zeroconf.h>
#include <libPlasma/c++/Plasma.h>
#include <libMedia/JpegImageClot.h>
#include <libAfferent/TwoHandBlurtRod.h>
#include <libAfferent/TwoHandProximityRod.h>
#include "ElectricalPoint.h"
#include <libGanglia/Ernestine.h>
#include <projects/quartermaster/TriptychConcierge.h>

#include <iostream>
#include <float.h>

// g-speak namespaces
using namespace oblong;
using namespace oblong::noodoo;
using namespace oblong::impetus;
using namespace oblong::gestation;
using namespace oblong::loam;
using namespace oblong::basement;
using namespace oblong::afferent;
using namespace oblong::twillig;
using namespace oblong::plasma;
using namespace oblong::media;
using namespace oblong::ganglia;
using namespace oblong::quartermaster;

#ifdef GQUERY
#include "../libEZ/gquery.h"
using namespace oblong::gquery;
#endif


class EZDrome  :  public VisiDrome
{
  OB_PRIVATE:
  EZDrome (Str name, const ObTrove <Str, UnspecifiedMemMgmt>& ot);
  static Ernestine *_global_ernestine;

  public:

  static Ernestine *GlobalErnestine ()
    { if (!_global_ernestine)
        { _global_ernestine = new Ernestine ();
          AppendAsTargetToDefaultInputs (_global_ernestine);
        }
      return _global_ernestine;
    }
  // globals
  // note that if you use pool names with tcp:// you will need to run pool_tcp_server
  static Str gripes_pool;  //  where all gripes come from
  static Str wands_pool;  //  wand data
  static int felds;  // number of felds already initialized
  static Gestator *tlg;  // top level gestator
  // these are the default input channels
  static KeyboardGTot *keyboard;  //  that thing with a lot of keys
  static MouseGTot *mouse;  //  that thing on the desktop
  static RoddedGlimpser *pointing;  //  for gloves
  static RoddedGlimpser *wanding;  //  for wands
  static RoddedGlimpser *displacement;  //  for displacement / pushback
  static Remote *remote;  //  for iOS app and javascript
  static BasicWandRollGlimpser *rolling;  // wand roll
  // HandiPoints are simple on-screen cursors for pointing gestures
  static ObTrove <HandiPoint *> handipoints;
  static ObTrove <ElectricalPoint *> electricalpoints;
  static bool perspectiveProjection;
  static ObColor feldBackingColor;
  static EZDrome *instance;
  static bool concierge_disabled;
  static TriptychConcierge *concierge;

  // frames per second logging class - it is automatically added
  //  to your application in InitVisiFeld ()
  class Secret  :  public ShowyThing,
                   public OEBlurt::Evts,
                   public OEPointing::Evts
  { PATELLA_SUBCLASS (Secret, ShowyThing);

     OB_PRIVATE:

      FatherTime *fps_timer;  //  g-speak timer
      int fps_frames;
      Vect mouse_origin, mouse_through;
      bool mouse_detected;
      Atmosphere *mouse_atm;
      Str mouse_provenance;

      Secret ();
      ObRetort OEPointingMove (OEPointingMoveEvent *e, Atmosphere *atm);
      ObRetort OEBlurtAppear (OEBlurtAppearEvent *e, Atmosphere *a);
      virtual ObRetort Travail (Atmosphere *atm);
  };

  static void SetDrawWithAlphaAndDepth (ShowyThing *st);
  static ObRetort EnableAlphaAndDepthTest (ShowyThing *st, VisiFeld *feld, Atmosphere *atm);
  static ObRetort DisableAlphaAndDepthTest (ShowyThing *st, VisiFeld *feld, Atmosphere *atm);
  static VisiFeld *Feld (int which);
  static void AppendToAllVisiFelds (KneeObject *ko);
  // this might result in the object being freed
  static void RemoveFromAllVisiFelds (KneeObject *ko);
  static EZDrome *Instance ();
  //  checks whether a pointing action crosses a digital object
  static bool WhackCheck (ShowyThing *st, OEPointingEvent *e);
  static Vect WrangledLoc (LocusThing *l);
  //  returns the point of intersection between
  //  the pointing line and the frontmost intersected feld plane
  static Vect IntersectionWithFeldPlane (OEPointingEvent *e, FlatThing *o = NULL);
  static Vect PrevIntersectionWithFeldPlane (OEPointingEvent *e);
  static Vect IntersectionWithGivenFeldPlane (OEPointingEvent *e, SpaceFeld *vf);
  static Vect IntersectionOfObjectWithGivenFeldPlane (Vect o, SpaceFeld *vf);
  //  returns the frontmost intersected feld
  static SpaceFeld *FrontmostIntersectedFeld (OEPointingEvent *e);
  static VidQuad *CreateSound (Str filename);
  //  returns the aspect ratio of the VisiFeld
  static float32 Aspect (VisiFeld *vf);
  //  returns the absolute point coordinates,
  //  given those relative to a given VisiFeld
  static Vect Abs (VisiFeld *vf, Vect relative);
  //  creates ten useful HandiPoint cursors
  static void CreateBunchOfHandiPoints (int how_many = 16, bool show_provenance = false);
  //  creates ten extremely useful ElectricalPoint cursors
  static void CreateBunchOfElectricalPoints (int how_many = 16, bool show_provenance = false);
  //  connects rod to glimpser and glimpser to event target
  static RoddedGlimpser *CreateGlimpser (EspyRod *rod, KneeObject *target = NULL);
  //  initializes a flat visual objects, either image (TexQuad) or video (VidQuad)
  // Note: InitQuad is deprecated. Call instead InitImageQuad or InitVidQuad.
  static void InitQuad (FlatThing *ft, VisiFeld *vf, Str filename = "", Str name = "noname",
        float perc = 1.0,
        ObColor color = ObColor (1.0, 1.0), bool softcolor = true);
  static void InitWebcamQuad (VidQuad *vq, VisiFeld *vf, Str pool_name, int width, int height, Str name = "noname");
  //  rotates an object (what)
  static void InitTexQuad (TexQuad *tq, VisiFeld *vf, Str filename = "", Str name = "noname",
        float perc = 1.0,
        ObColor color = ObColor (1.0, 1.0), bool softcolor = true);
  static void InitVidQuad (VidQuad *vq, VisiFeld *vf, Str filename = "", Str name = "noname",
        float perc = 1.0,
        ObColor color = ObColor (1.0, 1.0), bool softcolor = true);
  //  rotates an object (what)
  static RWrangler *MakeRotation (ShowyThing *what, const Vect &center, const Vect &axis, double angle); /* DEPRECATED */
  static RWrangler *MakeRotation (ShowyThing *what, SoftVect *sv_center, const Vect &axis, SoftFloat *sf_angle); /* DEPRECATED */
  static RWrangler *MakeRotator (ShowyThing *what, const Vect &center, const Vect &axis, double angle);
  static RWrangler *MakeRotator (ShowyThing *what, SoftVect *sv_center, const Vect &axis, SoftFloat *sf_angle);  // todo: what if I want a soft center and a float angle?
  //  scales an object (what)
  static SWrangler *MakeScale (ShowyThing *what, double scale); /* DEPRECATED */
  static SWrangler *MakeScale (ShowyThing *what, SoftVect *sv_scale); /* DEPRECATED */
  static SWrangler *MakeScaler (ShowyThing *what, double scale);
  static SWrangler *MakeScaler (ShowyThing *what, SoftVect *sv_scale);
  //  translates an object (what)
  static TWrangler *MakeTranslation (ShowyThing *what, Vect where); /* DEPRECATED */
  static TWrangler *MakeTranslation (ShowyThing *what, SoftVect *sv_translation); /* DEPRECATED */
  static TWrangler *MakeTranslator (ShowyThing *what, Vect where);
  static TWrangler *MakeTranslator (ShowyThing *what, SoftVect *sv_translation);
  //  initializes a VisiFeld
  static void InitVisiFeld (VisiFeld *vf, bool perspective,
        ObColor color = ObStyle::FeldBackingColor ());
  static void AppendAsTargetToDefaultInputs (KneeObject *t);
  //  draws a simple border around a FlatThing
  static void DrawQuadBorder (FlatThing *t, ObColor color, double lineWidth = 2.0);
  //  initializes a visual object containing text (GlyphoString)
  static void InitGlyphoString (GlyphoString *glyph, VisiFeld *vf, Str text, double size,
        Vect where, ObColor color = ObStyle::TextColor ());
  //  creates a visual object containing text (GlyphoString)
  static GlyphoString *CreateGlyphoString (VisiFeld *vf, Str text, double size,
    Vect where, ObColor color = ObStyle::TextColor ());
  static ObRetort FeldSetup (VisiDrome *drome, VisiFeld *vf, Atmosphere *atm);
  //  initializes a VisiDrome; basically this is usually
  //  all you need in your main ()
  static ObRetort CreateDrome (int ac, char **av, Str dromename,
        oblong::loam::ObRetort post_hook (oblong::noodoo::VFBunch*,
        oblong::basement::Atmosphere*), bool persp = false,
        ObColor fbc = ObStyle::FeldBackingColor ());
  //  initializes a hand pointing rod; you can redefine the gripes as you wish
  static OneHandPointingRod *CreateDefaultOneHandPointingRod (VisiFeld *vf);
  //  initializes a wand pointing rod
  static WandPointingRod *CreateDefaultWandPointingRod (VisiFeld *vf);
  static void SetupDefaultInputs (VisiFeld *vf, const Str &hostname_for_remote_pools = "");
  static void EmergencyExit ();

  static void DisableConcierge ();
  TriptychConcierge *Concierge ();
  void InitConcierge ();
};  // class EZDrome

#endif  // EZDROME_FOR_DUMMIES
