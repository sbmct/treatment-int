from flask import render_template
import os.path
from emcs_admin_utils import jsplasma
from emcs_admin_utils.flask_components import Component

#use flask because will do a lot of work for us
BASEPATH = os.path.abspath(os.path.dirname(__file__))
STATIC_PATH = os.path.join(BASEPATH, 'public')
TEMPLATES_PATH = os.path.join(BASEPATH, 'templates')
app = Component('streetcams', __name__, static_folder=STATIC_PATH, template_folder=TEMPLATES_PATH)


@app.route('/streetcams')
def streetcams_base():
    return render_template('streetcams.html', jsplasma=jsplasma)

