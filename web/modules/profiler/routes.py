from flask import render_template, url_for, redirect
import os.path
from emcs_admin_utils import jsplasma
from emcs_admin_utils.flask_components import Component
import plasma
from plasma.hose import Hose
from plasma.protein import Protein
import logging
logger = logging.getLogger(__name__)

#use flask because will do a lot of work for us
BASEPATH = os.path.abspath(os.path.dirname(__file__))
STATIC_PATH = os.path.join(BASEPATH, 'public')
TEMPLATES_PATH = os.path.join(BASEPATH, 'templates')
app = Component('profiler', __name__, static_folder=STATIC_PATH, template_folder=TEMPLATES_PATH)


@app.route('/profiler')
def profiler_base():
    return render_template('profiler.html', jsplasma=jsplasma)


@app.route('/profiler/track')
def profiler_track():
    #deposit a protein
    try:
        pool_name = 'track'
        #Hack because pyplasma does not work with mmap yet
        full_pool_name = 'tcp://%s/%s' % ('localhost', pool_name)
        hose = Hose.participate(full_pool_name)
        to_deposit = Protein(descrips=['profiler', 'track'],
                             ingests=plasma.slaw.from_json({}))
        hose.deposit(to_deposit)
    except plasma.exceptions.PoolNoSuchPoolException:
        logger.error('Profiler could not track, track pool does not exist')

    return redirect(url_for('sluice.base_sluice') + 'pmfProfile.htm')

