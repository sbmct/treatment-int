$(document).ready(function () {

  var TRACK_POOL = 'tcp://localhost/track';

  function track() {
    Plasma
    .Hose(TRACK_POOL)
    .Deposit({ descrips: ['profiler', 'track'],
               ingests: { } });
  }

   // Handles the button
  $("#track").click(function(e) {
    e.preventDefault();
    track();
  });

});
