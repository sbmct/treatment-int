$(document).ready(function () {

    var OUT_POOL = 'tcp://localhost/edge-to-sluice';
    var IN_POOL = 'tcp://localhost/sluice-to-edge';
    var TWITTER_POOL = 'tcp://localhost/twitter';

    function createFluoro(track) {
        Plasma
        .Hose(TWITTER_POOL)
        .Deposit({ descrips: ['twitter', 'track'],
                     ingests: { 'terms': track } });
    }

    function createWeb(url) {
        Plasma
        .Hose(OUT_POOL)
        .Deposit({ descrips: ['sluice', 'prot-spec v1.0', 'request', 'web'],
                     ingests: { 'windshield': [ { 'name': 'from_web '+new Date().toLocaleTimeString(),
                                 'url': url,
                                 'size': [0.8, 0.8] } ] } });
    }

    // Handles the button
    $("#create").click(function(e) {
        e.preventDefault();
        createFluoro($("#search").val());
        $("#search").val('');
        $("#search").focus();
    });

    // Handles the input box
    $("#search").keypress(function(e){
        var code = e.keyCode || e.which;
        if(code === 13) { //Enter keycode
            $("#create").click();
        }
    });

    // Handles the button
    $("#web_open").click(function(e) {
        e.preventDefault();
        createWeb($("#web").val());
        $("#web").focus();
    });

    // Handles the input box
    $("#web").keypress(function(e){
        var code = e.keyCode || e.which;
        if(code === 13) { //Enter keycode
            $("#web_open").click();
        }
    });
});
