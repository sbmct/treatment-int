function load_runner_config(runner) {

    var timeline = runner.timeline;
    // initialize time range
    timeline.setLiveTime();

    function traffic_cam_view(){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s,
                       runner.req_s, 'web'],
            ingests: {
                'url': 'http://www.tfl.gov.uk//tfl/livetravelnews/trafficcams/cctv/00001.01251.jpg',
                'auto-resize': false,
                'id': '1.01251',
                'size': [0.5, 0.5]
            }
        });
    };

    function show_dossier(){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s,
                       runner.req_s, 'web'],
            ingests: {
                "windshield": [
                    {
                        'name': 'Dossier',
                        'url': 'http://localhost:7787/sluice/profiler',
                        'feld': 'right',
                        'loc': [0.0, 0.29999999999999999],
                        'size': [0.40000000000000002, 0.9],
                    }
                ]
            }
        });
    };

    function track_cell_towers(){
        runner.PlasmaDeposit('track',{
            descrips: ['profiler','track'],
            ingests: {}
        });
    };

    function open_abbey_road(){
        runner.EdgeToSluiceDeposit({
            descrips: [runner.slc_s, runner.prs_s,
                       runner.req_s, 'web'],
            ingests: {
                "windshield": [
                    {
                        'url': 'http://localhost:7787/sluice/abbeyroad.html',
                        'size': [0.6, 0.6],
                        'loc': [0.0, -1.0],
                        'name': 'abbeyroad',
                        'feld': 'main'
                    }
                ]
            }
        });
    };


    function big_fluoro(name){
        runner.add_fluoro_args(name, { bl: [-65.0, -179.0],
                                       tr: [80.0, 179.0] });
    }

    //////////////////
    /// The Script ///
    //////////////////

    /// Start
    function act1(){
        timeline.setLiveTime();
        runner.clear_fluoroscopes();
        runner.bookmark(54.533650132811552, -2.78070309836806686, 20);
    }

    function act2(){
        timeline.setLiveTime();
        runner.clear_fluoroscopes();
        runner.bookmark(51.533650132811552, -0.18070309836806686, 4486.9466825786421);
    }

    function a2scene1(){
        runner.add_fluoro_args('Network',
                               { bl: [51.509359088216165, -0.23592047737582167],
                                 tr: [51.554056218199882, -0.10806807451679168] });
    }
    function a2scene2(){
        runner.add_fluoro_args('StreetCams',
                               { bl: [51.463645333853613, -0.34565931923117299],
                                 tr: [51.584181600464042, -0.00093419724002563598] });
    }
    function tweets(){
        runner.add_fluoro_args('Tweets',
                               { bl: [51.52417297072828, -0.16942635600910583],
                                 tr: [51.527248270956321, -0.16063087640728391] });
        //We might be able to get rid of thise
        runner.add_fluoro_args('Tweets',
                               { bl: [51.537866929931795, -0.19905737984577954],
                                 tr: [51.540732344607321, -0.19085973442529713] });
    }

    function act3(){
        runner.bookmark(51.533650132811552, -0.18070309836806686, 10000);
    }

    function fullZoomOut(){
        timeline.setTimeRangeMonthly();
        runner.bookmark(38.58, -95.14, 6.0); // zoom to full US
    }
    function fullZoomIn(){
        timeline.setTimeRangeHourly();
        runner.bookmark(36.368298, -94.224492, 89000.0, 10.0); 
    }

   //return methods to call

   //return methods to call
   //return methods to call
    runner_object = {
        'fzout': ['fzout', fullZoomOut],
        'fzin': ['fzin', fullZoomIn],
        'act1': ['START', act1],
        'act2': ['London', act2],
        'a2scene1': ['Mobile Network', a2scene1],
        'a2scene2': ['Traffic Cams', a2scene2],
        'a2scene3': ['Camera View', traffic_cam_view],
        'a2scene4': ['Tweets', tweets],
        'act3': ['Tracking', act3],
        'a3scene1': ['Show Dossier', show_dossier],
        'a3scene2': ['Track Cell Towers', track_cell_towers],
        'a3scene3': ['Abbey Cam', open_abbey_road]
    }
    return runner_object;

}

