from flask import Blueprint, render_template
from emcs_admin_utils import jsplasma

intrunner = Blueprint('intrunner', __name__,
                       static_folder = 'static',
                       template_folder = 'templates')

@intrunner.route('/')
def base():
    return render_template('runner.html', jsplasma = jsplasma)
