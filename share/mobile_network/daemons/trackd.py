"""
On click of a dossier, send a set of proteins on the track pool
"""
import argparse
import time
import os.path
from cplasma.metabolizer import Metabolizer
from cplasma import Hose
from cplasma import yamlio

DEFAULT_OUTPOOL = 'topo-to-sluice'
DEFAULT_INPOOL = 'track'
OBS_PATH = os.path.join(os.path.dirname(__file__), '..', 'obs')


class TrackDaemon(object):
    def __init__(self, inpool, outpool):
        self._inpool = inpool
        self._outpool = outpool
        self._metabo = Metabolizer()
        self._metabo.poolParticipate(self._inpool)
        self._metabo.poolParticipate(self._outpool)
        self._metabo.appendMetabolizer(['profiler', 'track'], self.process_poke, 'start-track')
        self._metabo.appendMetabolizer(['profiler', 'reset'], self.reset_poke, 'reset-track')

    def run(self):
        #TODO: Could run create the pools temporarily? or maybe in __init__
        self._metabo.metabolize()

    def _send_protein(self, i):
        path = os.path.join(OBS_PATH, '%d.yaml' % i)
        #The ruby code calls parse_yaml_proteins, but every file only has 1 protein
        prot = yamlio.parse_yaml_protein(open(path))
        hose = Hose.participate(self._outpool)
        #TODO: RProtein probably should not hide protein, in order to avoid
        #building a protein from ingests and descrips, just access __protein
        #directly
        hose.deposit(prot._RProtein__protein)

    def process_poke(self, protein):
        for i in xrange(1, 16+1):
            self._send_protein(i)
            time.sleep(2)

    def reset_poke(self, protein):
        self._send_protein(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Send obs simulating tracking a person of interest',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--outpool', '-o', type=str, default=DEFAULT_OUTPOOL,
                        help="Obs-To-Sluice Pool")
    parser.add_argument('--inpool', '-i', type=str, default=DEFAULT_INPOOL,
                        help="Obs-To-Sluice Pool")
    args = parser.parse_args()

    tracker = TrackDaemon(args.inpool, args.outpool)
    tracker.run()

