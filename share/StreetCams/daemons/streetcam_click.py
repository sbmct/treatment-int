from sluice import Sluice
import sys, time, uuid, os, json, traceback
import logging
from itertools import islice
import warnings
import urllib

from sluice.types import (float64, int64, obbool, v2float64, v3float64,
    unt8, oblist, numeric_array, numeric_array_from_file, obcons)
from plasma.const import POOL_WAIT_FOREVER
from plasma import Protein, Hose, HoseGang
from plasma.exceptions import *
#convert protein to yaml for file output
from plasma.yamlio import dump_yaml_protein
from sluice.entities import *

PYSLUICE_VERSION = '1.1.3'

APP = 'sluice'
PROTO = 'prot-spec v1.0'
UPDATE_LAG = 0.25
HEARTBEAT_POOL        = 'sluice-to-heart'
EDGE_REQUEST_POOL     = 'edge-to-sluice'
EDGE_RESPONSE_POOL    = 'sluice-to-edge'
class StreetCamFluoro(Sluice):
    edge_listen = True
    def handle_poked_it(self, p):
        ing = p.ingests()
        if 'StreetCams' == ing.get('kind', None):
            url = ing.get('attrs', {}).get('url', None)
            ident = ing.get('id', None)
            if url is not None and ident is not None:
                self.request_web_fluoro('street cam' + ident, url, placement_entity_id=ident)

def main():
    s = StreetCamFluoro()
    s.run()
    s.quit()

if '__main__' == __name__:
    main()
